#ifndef BLOCO_H
#define BLOCO_H

#include <cmath>
#include "../interface/Objeto.h"
#include "../estruturas/solidosDecomposicao.h"

class Bloco : virtual public Objeto {
private:
    double largura, altura, profundidade;

public:
    Bloco (Eigen::Vector3d centro, double largura, double altura, double profundidade);

    double& obterLargura ();
    double& obterAltura ();
    double& obterProfundidade ();
    double& obterMaiorDimensao ();
    Eigen::Vector3d& obterCentro ();

    int classificar (Eigen::Vector3d pos, double tam);
    bool intercessao (Eigen::Vector3d &vetor, Eigen::Vector3d &observador);
    Eigen::Vector3d posIntercessao (Eigen::Vector3d &vetor, Eigen::Vector3d &observador);
    Eigen::Vector3d calcularCor (Eigen::Vector3d &vetor, Eigen::Vector3d &luzGlobal);
};

#endif