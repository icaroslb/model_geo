#include "Face.h"

Face::Face (Vertice *vert1, Vertice *vert2, Vertice *vert3) {
    pontos[0] = vert1;
    pontos[1] = vert2;
    pontos[2] = vert3;

    normal = (pontos[1]->posicao - pontos[0]->posicao).cross(pontos[2]->posicao - pontos[1]->posicao).normalized();
}

Vertice* Face::operator [] (int indice) {
    return pontos[indice];
}

int Face::classificar (Eigen::Vector3d pos, double tam) {
    Eigen::Vector3d vNPerto = (pontoMaisProximo(normal, tam) + pos) - pontos[0]->posicao,
                    vNLonge = (pontoMaisLonge(normal, tam) + pos) - pontos[0]->posicao,
                    v1 = (pontos[0]->posicao - pos).cwiseAbs(),
                    v2 = (pontos[1]->posicao - pos).cwiseAbs(),
                    v3 = (pontos[2]->posicao - pos).cwiseAbs(),
                    x = pontos[1]->posicao - pontos[0]->posicao,
                    z = pontos[2]->posicao - pontos[0]->posicao;
    
    if ((v1.x() < tam && v1.y() < tam && v1.z() < tam) ||
        (v2.x() < tam && v2.y() < tam && v2.z() < tam) ||
        (v3.x() < tam && v3.y() < tam && v3.z() < tam)) {
            return 0;
    } else {
        if (vNPerto.dot(normal) < 0 && vNLonge.dot(normal) < 0) {
            return 1;
        } else if (vNPerto.dot(normal) > 0 && vNLonge.dot(normal) > 0) {
            return -1;
        } else {
            Eigen::MatrixXd mudanca(3, 3);

            mudanca << x.x(), normal.x(), z.x(),
                       x.y(), normal.y(), z.y(),
                       x.z(), normal.z(), z.z();

            mudanca = mudanca.inverse();
            vNPerto = mudanca * vNPerto;
            
            if (vNPerto.x() > 0 && vNPerto.x() < 1 &&
                vNPerto.z() > 0 && vNPerto.z() < 1) {
                return 0;
            } else {
                return -1;
            }
        }
    }
}

void mostrarFaces (std::vector<Face*> &faces) {
    glBegin(GL_TRIANGLES);
    for (Face *f : faces) {
        glVertex3dv(f->pontos[0]->posicao.data());
        glVertex3dv(f->pontos[1]->posicao.data());
        glVertex3dv(f->pontos[2]->posicao.data());
    }
    glEnd();

    glBegin(GL_LINES);
    for (Face *f : faces) {
        Eigen::Vector3d normal = f->pontos[0]->posicao + f->normal;
        glVertex3dv(f->pontos[0]->posicao.data());
        glVertex3dv(normal.data());
    }
    glEnd();
}