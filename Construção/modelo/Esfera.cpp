#include "Esfera.h"

Esfera::Esfera (Eigen::Vector3d centr, double rai) : Objeto(centr){
    raio = rai;
}

double& Esfera::obterLargura (){
    return raio;
}

double& Esfera::obterAltura (){
    return raio;
}

Eigen::Vector3d& Esfera::obterCentro (){
    return centro;
}

int Esfera::classificar (Eigen::Vector3d pos, double tam){
    Eigen::Vector3d vDistCentro = pos - centro,
                    vDistMenor = (pontoMaisProximo(vDistCentro, tam) + pos) - centro,
                    vDistMaior = (pontoMaisLonge(vDistCentro, tam) + pos) - centro;
        
    if (vDistCentro.norm() <= raio) {
        if (vDistMaior.norm() <= raio) {
            return 1;
        } else {
            return 0;
        }
    } else {
        if (vDistMenor.norm() < raio) {
            return 0;
        } else {
            return -1;
        }
    }
}

bool Esfera::intercessao (Eigen::Vector3d &vetor, Eigen::Vector3d &observador){

}

Eigen::Vector3d Esfera::posIntercessao (Eigen::Vector3d &vetor, Eigen::Vector3d &observador){

}

Eigen::Vector3d Esfera::calcularCor (Eigen::Vector3d &vetor, Eigen::Vector3d &luzGlobal){

}
