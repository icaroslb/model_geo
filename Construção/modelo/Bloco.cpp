#include "Bloco.h"

Bloco::Bloco (Eigen::Vector3d centro, double largura, double altura, double profundidade) : Objeto(centro){
    this->largura = largura;
    this->altura = altura;
    this->profundidade = profundidade;
}

double& Bloco::obterLargura () {
    return largura;
}

double& Bloco::obterAltura () {
    return altura;
}

double& Bloco::obterProfundidade () {
    return profundidade;
}

double& Bloco::obterMaiorDimensao () {
    return (largura > altura && largura > profundidade) ? largura : (altura > profundidade) ? altura : profundidade;
}

Eigen::Vector3d& Bloco::obterCentro (){
    return centro;
}

int Bloco::classificar (Eigen::Vector3d pos, double tam){
    Eigen::Vector3d vDistCentro = (pos - centro),
                    vPontoProximo = ((pontoMaisProximo(vDistCentro, tam) + pos) - centro).cwiseAbs(),
                    vPontoLonge = ((pontoMaisLonge(vDistCentro, tam) + pos) - centro).cwiseAbs();
    vDistCentro = vDistCentro.cwiseAbs();

    if (vDistCentro.x() <= largura && vDistCentro.y() <= altura && vDistCentro.z() <= profundidade) {
        if(vPontoLonge.x() <= largura && vPontoLonge.y() <= altura && vPontoLonge.z() <= profundidade) {
            return 1;
        } else {
            return 0;
        }
    } else {
        if (vPontoProximo.x() < largura && vPontoProximo.y() < altura && vPontoProximo.z() < profundidade) {
            return 0;
        } else {
            return -1;
        }
    }
}

bool Bloco::intercessao (Eigen::Vector3d &vetor, Eigen::Vector3d &observador){

}

Eigen::Vector3d Bloco::posIntercessao (Eigen::Vector3d &vetor, Eigen::Vector3d &observador){

}

Eigen::Vector3d Bloco::calcularCor (Eigen::Vector3d &vetor, Eigen::Vector3d &luzGlobal){

}
