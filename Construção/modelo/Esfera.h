#ifndef ESFERA_H
#define ESFERA_H

#include "../interface/Objeto.h"
#include "../estruturas/solidosDecomposicao.h"

class Esfera : virtual public Objeto {
private:
    double raio;

public:
    Esfera (Eigen::Vector3d centr, double rai);

    double& obterLargura ();
    double& obterAltura ();
    Eigen::Vector3d& obterCentro ();

    int classificar (Eigen::Vector3d pos, double tam);
    bool intercessao (Eigen::Vector3d &vetor, Eigen::Vector3d &observador);
    Eigen::Vector3d posIntercessao (Eigen::Vector3d &vetor, Eigen::Vector3d &observador);
    Eigen::Vector3d calcularCor (Eigen::Vector3d &vetor, Eigen::Vector3d &luzGlobal);
};

#endif