#ifndef FACE_H
#define FACE_H

#include <GL/gl.h>
#include <vector>
#include "Vertice.h"

class Face {
public:
    Vertice *pontos[3];
    Eigen::Vector3d normal;
    
    Face(Vertice *vert1, Vertice *vert2, Vertice *vert3);

    Vertice* operator [] (int indice);
    int classificar (Eigen::Vector3d pos, double tam);
};

void mostrarFaces (std::vector<Face*> &faces);

#include "../estruturas/solidosDecomposicao.h"

#endif