#ifndef OBJETO_H
#define OBJETO_H

#include "../Eigen/Dense"

class Objeto {
protected:
    Eigen::Vector3d centro;

public:
    Objeto (Eigen::Vector3d centr) {centro = centr;}

    virtual double& obterLargura () = 0;
    virtual double& obterAltura () = 0;
    virtual Eigen::Vector3d& obterCentro () = 0;

    virtual int classificar (Eigen::Vector3d pos, double tam) = 0;
    virtual bool intercessao (Eigen::Vector3d &vetor, Eigen::Vector3d &observador) = 0;
    virtual Eigen::Vector3d posIntercessao (Eigen::Vector3d &vetor, Eigen::Vector3d &observador) = 0;
    virtual Eigen::Vector3d calcularCor (Eigen::Vector3d &vetor, Eigen::Vector3d &luzGlobal) = 0;
};

#endif //OBJETO_H
