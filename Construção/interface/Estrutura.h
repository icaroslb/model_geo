#ifndef ESTRUTURA_H
#define ESTRUTURA_H

#include "../Eigen/Dense"

template <typename T>
class Estrutura {
public:
    virtual bool intersecao (Eigen::Vector3d &vetor, Eigen::Vector3d &observador);
    Eigen::Vector3d posIntercessao (Eigen::Vector3d &vetor, Eigen::Vector3d &observador);
    virtual Eigen::Vector3d calcularCor (Eigen::Vector3d &vetor, Eigen::Vector3d &luzGlobal);
    
    virtual Estrutura diferenca (Estrutura &estr);
    virtual Estrutura uniao (Estrutura &estr);
    virtual Estrutura intersecao (Estrutura &estr);

    virtual void aplicar ();
};

#endif //ESTRUTURA_H