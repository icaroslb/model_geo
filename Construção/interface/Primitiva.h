#ifndef PRIMITIVA_H
#define PRIMITIVA_H

#include "../Eigen/Dense"

class Primitiva {
public:
    virtual void aplicar (Eigen::MatrixXd matriz);
    virtual bool intercessao (Eigen::Vector3d &vetor, Eigen::Vector3d &observador);
};

#endif