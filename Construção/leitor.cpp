#include "leitor.h"

std::vector<Face*> leitorObj(std::string arquivo) {
    std::vector<Vertice*> vertices;
    std::vector<Face*> faces;
    std::vector<std::string*> infor;
    std::ifstream arq;
    std::string linha;

    arq.open(arquivo);
    while (getline(arq, linha)) {
        if (!linha.substr(0, 2).compare("v ")) {
            infor = split(linha.substr(2), ' ');
            vertices.push_back(new Vertice(Eigen::Vector3d(std::stof(*infor[0]), std::stof(*infor[1]), std::stof(*infor[2]))));
            
        } else if (!linha.substr(0, 2).compare("f ")) {
            infor = split(linha.substr(2), ' ');
            int v1 = std::stoi(*infor[0]) - 1,
                v2 = std::stoi(*infor[1]) - 1,
                v3 = std::stoi(*infor[2]) - 1;
            faces.push_back(new Face(vertices[v1], vertices[v2], vertices[v3]));
        }
    }

    arq.close();
    return faces;
}

std::vector<std::string*> split (std::string linha, char separador) {
    std::vector<std::string*> respo;
    int ultOcorrencia  = 0;

    for (int i = 0; i < linha.size(); i++) {
        if (linha[i] == separador) {
            respo.push_back(new std::string(linha.substr(ultOcorrencia, i - ultOcorrencia)));
            ultOcorrencia += i - ultOcorrencia + 1;
        }
    }
    respo.push_back(new std::string(linha.substr(ultOcorrencia, linha.size() - ultOcorrencia)));

    return respo;
}

void exportar (std::string arquivo, No *arvore, float tam, Eigen::Vector3d centro, std::ofstream *arq) {
    bool aberto = false;
    if (arq == NULL) {
        arq = new std::ofstream(arquivo);
        *arq << 1 << ' ' << centro.x() << ' ' << centro.y() << ' ' << centro.z() << ' ' << tam;
        aberto = true;
    }

    if (arvore == NULL) {
        *arq << " 0";
    } else if (arvore->obterEstado() == PRETO) {
        *arq << " 1";
    } else {
        *arq << " 2";
        exportar(arquivo, arvore->filho(1), tam, centro, arq);
        exportar(arquivo, arvore->filho(2), tam, centro, arq);
        exportar(arquivo, arvore->filho(3), tam, centro, arq);
        exportar(arquivo, arvore->filho(0), tam, centro, arq);
        exportar(arquivo, arvore->filho(7), tam, centro, arq);
        exportar(arquivo, arvore->filho(4), tam, centro, arq);
        exportar(arquivo, arvore->filho(5), tam, centro, arq);
        exportar(arquivo, arvore->filho(6), tam, centro, arq);
    }

    if (aberto) {
        arq->close();
    }
}