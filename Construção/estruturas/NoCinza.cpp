#include "NoCinza.h"

NoCinza::NoCinza () : No(CINZA) {
    parentes.resize(8);
}

NoCinza::NoCinza (std::vector<No*> novosParentes) : No(CINZA) {
    parentes = novosParentes;
}

No* NoCinza::filho (int ind) {
    return parentes[ind];
}

void NoCinza::incluir (int ind, No *novoFilho) {
    parentes[ind] = novoFilho;
}

void NoCinza::operator = (std::vector<No*> novosParentes) {
    parentes = novosParentes;
}

No* NoCinza::retirar (int ind) {
    No* respo = parentes[ind];
    parentes[ind] = NULL;

    return respo;
}

void NoCinza::aplicar (Eigen::MatrixXd transf) {
    
}

void NoCinza::mostrar () {
    for (No *i : parentes) {
        if (i != NULL) {
            i->mostrar();
        }
    }
}

double NoCinza::calcularVolume () {
    double volume = 0;
    for (int i = 0; i < 8; i++) {
        if (parentes[i] != NULL) {
            volume += parentes[i]->calcularVolume();
        }
    }

    return volume;
}
