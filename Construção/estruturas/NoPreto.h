#ifndef NO_PRETO
#define NO_PRETO

#include "No.h"
#include <cstring>

class NoPreto : virtual public No {
    Eigen::Vector3d pos;
    double tam;
public:
    NoPreto (double x, double y, double z, double tamanho);
    NoPreto (Eigen::Vector3d posicao, double tamanho);

    No* filho (int ind);
    Eigen::Vector3d& obterPosicao ();
    void aplicar (Eigen::MatrixXd transf);

    void mostrar ();
    double calcularVolume ();
    double& tamanho ();
    Eigen::Vector3d& posicao ();
};

#endif
