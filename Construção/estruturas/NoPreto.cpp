#include "NoPreto.h"

NoPreto::NoPreto (double x, double y, double z, double tamanho) : No(PRETO), pos(x, y ,z), tam(tamanho) {
}

NoPreto::NoPreto (Eigen::Vector3d posicao, double tamanho) : No(PRETO), pos(posicao), tam(tamanho) {
}

No* NoPreto::filho (int ind) {
    return nullptr;
}

Eigen::Vector3d& NoPreto::obterPosicao () {
    return pos;
}

void NoPreto::aplicar (Eigen::MatrixXd transf) {
    
}

void NoPreto::mostrar () {
    Eigen::Vector3d vec0 = pos + Eigen::Vector3d(-tam, tam, -tam),
                    vec1 = pos + Eigen::Vector3d(-tam, -tam, -tam),
                    vec2 = pos + Eigen::Vector3d(tam, -tam, -tam),
                    vec3 = pos + Eigen::Vector3d(tam, tam, -tam),
                    vec4 = pos + Eigen::Vector3d(tam, -tam, tam),
                    vec5 = pos + Eigen::Vector3d(tam, tam, tam),
                    vec6 = pos + Eigen::Vector3d(-tam, tam, tam),
                    vec7 = pos + Eigen::Vector3d(-tam, -tam, tam);

    //quadrado 1
    glBegin(GL_QUADS);
    glVertex3dv(vec0.data());
    glVertex3dv(vec1.data());
    glVertex3dv(vec2.data());
    glVertex3dv(vec3.data());
    
    //quadrado 2
    
    glVertex3dv(vec3.data());
    glVertex3dv(vec2.data());
    glVertex3dv(vec4.data());
    glVertex3dv(vec5.data());
    
    //quadrado 3
    
    glVertex3dv(vec4.data());
    glVertex3dv(vec5.data());
    glVertex3dv(vec6.data());
    glVertex3dv(vec7.data());
    
    //quadrado 4
    
    glVertex3dv(vec6.data());
    glVertex3dv(vec7.data());
    glVertex3dv(vec1.data());
    glVertex3dv(vec0.data());
    
    //quadrado 5
    
    glVertex3dv(vec6.data());
    glVertex3dv(vec0.data());
    glVertex3dv(vec3.data());
    glVertex3dv(vec5.data());
    
    //quadrado 6
    
    glVertex3dv(vec4.data());
    glVertex3dv(vec2.data());
    glVertex3dv(vec1.data());
    glVertex3dv(vec7.data());
    glEnd();
}

double NoPreto::calcularVolume () {
    return tam * tam * tam;
}

double& NoPreto::tamanho () {
    return tam;
}

Eigen::Vector3d& NoPreto::posicao () {
    return pos;
}