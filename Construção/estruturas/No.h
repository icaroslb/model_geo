#ifndef NO
#define NO

#include "../Eigen/Dense"
#include <GL/gl.h>

#define PRETO 1
#define CINZA 0
#define BRANCO -1

class No {
protected:
    int estado;

public:
    No(int est) {estado = est;}
    int obterEstado () { return estado; }
    virtual No* filho (int ind) = 0;
    virtual void aplicar (Eigen::MatrixXd transf) = 0;
    virtual void mostrar () = 0;
    virtual double calcularVolume () = 0;
    virtual double& tamanho () = 0;
    virtual Eigen::Vector3d& posicao () = 0;
};

#endif
