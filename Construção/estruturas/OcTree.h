#ifndef OCTREE
#define OCTREE

#include "../interface/Estrutura.h"
#include "../interface/Objeto.h"
#include "NoPreto.h"
#include "NoCinza.h"

class OcTree {
public:
    No *raiz;
    
    OcTree ();
    OcTree (No *raiz);

    No* obterRaiz ();
    void mostrar ();
    double calcularVolume ();

    void aplicar (Eigen::MatrixXd transf);
};

#endif
