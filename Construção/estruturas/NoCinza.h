#ifndef NO_CINZA
#define NO_CINZA

#include <vector>
#include "No.h"

class NoCinza : virtual public No {
public:
    std::vector<No*> parentes;

    NoCinza ();
    NoCinza (std::vector<No*> novosParentes);

    No* filho (int ind);
    void incluir (int ind, No *novoFilho);
    void operator = (std::vector<No*> novosParentes);
    No* retirar (int ind);

    void aplicar (Eigen::MatrixXd transf);
    void mostrar ();
    double calcularVolume ();
    double& tamanho () {}
    Eigen::Vector3d& posicao () {}
};

#endif
