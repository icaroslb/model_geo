#include "OcTree.h"

OcTree::OcTree () {
    this->raiz = NULL;
}

OcTree::OcTree (No *raiz) {
    this->raiz = raiz;
}

No* OcTree::obterRaiz () {
    return raiz;
}

void OcTree::mostrar () {
    if (raiz != NULL) {
        raiz->mostrar();
    }
}

double OcTree::calcularVolume () {
    if (raiz != NULL) {
        return raiz->calcularVolume();
    } else {
        return 0;
    }
}

void aplicar (Eigen::MatrixXd transf) {
    
}