#include "solidosDecomposicao.h"

No* construir (Objeto *obj, Eigen::Vector3d centro, double tam, int profundidadeMax, int profundidade) {
    if (profundidade > profundidadeMax-1) {
        return new NoPreto(centro, tam);
    }

    int classificacao = obj->classificar(centro, tam);
    profundidade++;

    if (classificacao == PRETO) {
        return new NoPreto(centro, tam);
    } else if (classificacao == CINZA) {
        tam /= 2;

        std::vector<No*> parentes(8);
        Eigen::Vector3d oct0(-tam, tam, tam),
                        oct1(-tam, -tam, tam),
                        oct2(tam, -tam, tam),
                        oct3(tam, tam, tam);

        parentes[0] = construir(obj, centro + oct0, tam, profundidadeMax, profundidade);
        parentes[1] = construir(obj, centro + oct1, tam, profundidadeMax, profundidade);
        parentes[2] = construir(obj, centro + oct2, tam, profundidadeMax, profundidade);
        parentes[3] = construir(obj, centro + oct3, tam, profundidadeMax, profundidade);
        parentes[4] = construir(obj, centro - oct0, tam, profundidadeMax, profundidade);
        parentes[5] = construir(obj, centro - oct1, tam, profundidadeMax, profundidade);
        parentes[6] = construir(obj, centro - oct2, tam, profundidadeMax, profundidade);
        parentes[7] = construir(obj, centro - oct3, tam, profundidadeMax, profundidade);

        for (int i = 0; i < 8; i++) {
            if (parentes[i] == NULL || parentes[i]->obterEstado() == CINZA) {
                return new NoCinza (parentes);
            }
        }

        return new NoPreto(centro, 2*tam);
    } else {
        return NULL;
    }
}

void construirI (Objeto *obj, OcTree &arvore, Eigen::Vector3d centro, double tam, int profundidadeMax, int profundidade, No **raiz) {
    if (raiz == NULL) {
        raiz = &arvore.raiz;
    }

    if (profundidade > profundidadeMax-1) {
        *raiz = new NoPreto(centro, tam);
        return;
    }

    int classificacao = obj->classificar(centro, tam);
    profundidade++;

    if (classificacao == PRETO) {
        *raiz = new NoPreto(centro, tam);
        return;
    } else if (classificacao == CINZA) {
        tam /= 2;

        //std::vector<std::thread> parentes;
        Eigen::Vector3d oct0(-tam, tam, tam),
                        oct1(-tam, -tam, tam),
                        oct2(tam, -tam, tam),
                        oct3(tam, tam, tam);
        NoCinza *novoNo = new NoCinza();
        No **filhos = novoNo->parentes.data();

        construirI(obj, std::ref(arvore), centro + oct0, tam, profundidadeMax, profundidade, filhos);
        construirI(obj, std::ref(arvore), centro + oct1, tam, profundidadeMax, profundidade, &filhos[1]);
        construirI(obj, std::ref(arvore), centro + oct2, tam, profundidadeMax, profundidade, &filhos[2]);
        construirI(obj, std::ref(arvore), centro + oct3, tam, profundidadeMax, profundidade, &filhos[3]);
        construirI(obj, std::ref(arvore), centro - oct0, tam, profundidadeMax, profundidade, &filhos[4]);
        construirI(obj, std::ref(arvore), centro - oct1, tam, profundidadeMax, profundidade, &filhos[5]);
        construirI(obj, std::ref(arvore), centro - oct2, tam, profundidadeMax, profundidade, &filhos[6]);
        construirI(obj, std::ref(arvore), centro - oct3, tam, profundidadeMax, profundidade, &filhos[7]);
        
        for (int i = 0; i < 8; i++) {
            if (filhos[i] == NULL || filhos[i]->obterEstado() == CINZA) {
                *raiz = novoNo;
                return;
            }
        }

        *raiz = new NoPreto(centro, 2*tam);
        delete novoNo;
        return;
    } else {
        *raiz = NULL;
        return;
    }
}

No* construir (std::vector<Face*> *obj, Eigen::Vector3d centro, double tam, int profundidadeMax, int profundidade) {
    std::vector<Face*> novasFaces;
    int classificacao,
        dentro = 0;

    if (profundidade > profundidadeMax-1) {
        return new NoPreto(centro, tam);
    }

    for (Face *f : *obj) {
        classificacao = f->classificar(centro, tam);

        if (classificacao == 0) {
            novasFaces.push_back(f);
        } else if (classificacao == -1) {
            dentro++;
        }
    }

    if (novasFaces.size() > 0 && dentro < obj->size()) {
        profundidade++;
        tam /= 2;

        std::vector<No*> parentes(8);
        Eigen::Vector3d oct0(-tam, tam, tam),
                        oct1(-tam, -tam, tam),
                        oct2(tam, -tam, tam),
                        oct3(tam, tam, tam);

        parentes[0] = construir(&novasFaces, centro + oct0, tam, profundidadeMax, profundidade);
        parentes[1] = construir(&novasFaces, centro + oct1, tam, profundidadeMax, profundidade);
        parentes[2] = construir(&novasFaces, centro + oct2, tam, profundidadeMax, profundidade);
        parentes[3] = construir(&novasFaces, centro + oct3, tam, profundidadeMax, profundidade);
        parentes[4] = construir(&novasFaces, centro - oct0, tam, profundidadeMax, profundidade);
        parentes[5] = construir(&novasFaces, centro - oct1, tam, profundidadeMax, profundidade);
        parentes[6] = construir(&novasFaces, centro - oct2, tam, profundidadeMax, profundidade);
        parentes[7] = construir(&novasFaces, centro - oct3, tam, profundidadeMax, profundidade);

        for (int i = 0; i < 8; i++) {
            if (parentes[i] == NULL || parentes[i]->obterEstado() == CINZA) {
                return new NoCinza (parentes);
            }
        }

        return new NoPreto(centro, 2*tam);
    } else if (dentro == 0) {
        return new NoPreto(centro, tam);
    } else {
        return NULL;
    }

}

void gerarPontos(double pontos[8][4], double centro[4], double tam) {
    for (int i = 0; i < 8; i++) {
        memcpy(pontos[i], centro, 4);
    }
}

Eigen::Vector3d pontoMaisProximo (Eigen::Vector3d vDist, double tam) {
    double x = (vDist.x() > 0) ? -tam : tam,
           y = (vDist.y() > 0) ? -tam : tam,
           z = (vDist.z() > 0) ? -tam : tam;
    Eigen::Vector3d respo(x, y, z);

    return respo;
}

Eigen::Vector3d pontoMaisProximo (Eigen::Vector3d vDist, double largura, double altura, double profundidade) {
    double x = (vDist.x() > 0) ? -largura : largura,
           y = (vDist.y() > 0) ? -altura : altura,
           z = (vDist.z() > 0) ? -profundidade : profundidade;
    Eigen::Vector3d respo(x, y, z);

    return respo;
}

Eigen::Vector3d pontoMaisLonge (Eigen::Vector3d vDist, double tam) {
    double x = (vDist.x() > 0) ? tam : -tam,
           y = (vDist.y() > 0) ? tam : -tam,
           z = (vDist.z() > 0) ? tam : -tam;
    Eigen::Vector3d respo(x, y, z);

    return respo;
}

Eigen::Vector3d pontoMaisLonge (Eigen::Vector3d vDist, double largura, double altura, double profundidade) {
    double x = (vDist.x() > 0) ? largura : -largura,
           y = (vDist.y() > 0) ? altura : -altura,
           z = (vDist.z() > 0) ? profundidade : -profundidade;
    Eigen::Vector3d respo(x, y, z);

    return respo;
}

No* uniao (No* arvore1, No* arvore2) {
    if (arvore1 != NULL && arvore2 != NULL) {
        if (arvore1->obterEstado() == PRETO || arvore2->obterEstado() == PRETO) {
            return (arvore1->obterEstado() == PRETO) ? arvore1 : arvore2;
        } else {
            NoCinza *respo = new NoCinza();
            respo->parentes[0] = uniao(arvore1->filho(0), arvore2->filho(0));
            respo->parentes[1] = uniao(arvore1->filho(1), arvore2->filho(1));
            respo->parentes[2] = uniao(arvore1->filho(2), arvore2->filho(2));
            respo->parentes[3] = uniao(arvore1->filho(3), arvore2->filho(3));
            respo->parentes[4] = uniao(arvore1->filho(4), arvore2->filho(4));
            respo->parentes[5] = uniao(arvore1->filho(5), arvore2->filho(5));
            respo->parentes[6] = uniao(arvore1->filho(6), arvore2->filho(6));
            respo->parentes[7] = uniao(arvore1->filho(7), arvore2->filho(7));
            return respo;
        }
    } else {
        return (arvore1 == NULL) ? arvore2 : arvore1;
    }
}

void escalonar (No* raiz, float escalonamento, Eigen::Vector3d pos) {
    if (raiz == NULL) {
        return;
    } else if (raiz->obterEstado() == PRETO) {
        Eigen::MatrixXd escalar(3, 3);
        escalar << escalonamento / raiz->tamanho(), 0, 0,
                     0, escalonamento / raiz->tamanho(), 0,
                     0, 0, escalonamento / raiz->tamanho();
        raiz->posicao() = raiz->posicao() - pos;
        raiz->posicao() = escalar * raiz->posicao();
        raiz->posicao() = raiz->posicao() + pos;
        raiz->tamanho() = escalonamento;
    } else {
        escalonamento /= 2;
        escalonar(raiz->filho(0), escalonamento, pos);
        escalonar(raiz->filho(1), escalonamento, pos);
        escalonar(raiz->filho(2), escalonamento, pos);
        escalonar(raiz->filho(3), escalonamento, pos);
        escalonar(raiz->filho(4), escalonamento, pos);
        escalonar(raiz->filho(5), escalonamento, pos);
        escalonar(raiz->filho(6), escalonamento, pos);
        escalonar(raiz->filho(7), escalonamento, pos);
    }
}