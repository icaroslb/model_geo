#ifndef SOLIDOS_DECOMPOSICAO
#define SOLIDOS_DECOMPOSICAO

#include <thread>
#include "OcTree.h"
#include "../modelo/Face.h"

No* construir (Objeto *obj, Eigen::Vector3d centro, double tam, int profundidadeMax, int profundidade = 0);
void construirI (Objeto *obj, OcTree &arvore, Eigen::Vector3d centro, double tam, int profundidadeMax, int profundidade = 0, No **raiz = nullptr);
No* construir (std::vector<Face*> *obj, Eigen::Vector3d centro, double tam, int profundidadeMax, int profundidade = 0);

Eigen::Vector3d pontoMaisProximo (Eigen::Vector3d vDist, double tam);
Eigen::Vector3d pontoMaisProximo (Eigen::Vector3d vDist, double largura, double altura, double profundidade);
Eigen::Vector3d pontoMaisLonge (Eigen::Vector3d vDist, double tam);
Eigen::Vector3d pontoMaisLonge (Eigen::Vector3d vDist, double largura, double altura, double profundidade);

No* uniao (No* arvore1, No* arvore2);
void escalonar (No* raiz, float escalonamento, Eigen::Vector3d pos);

#endif