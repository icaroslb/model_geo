#ifndef LEITOR_H
#define LEITOR_H

#include <vector>
#include <fstream>
#include <string>
#include <iostream>
#include "modelo/Face.h"
#include "Eigen/Dense"

#include "modelo/Face.h"
#include "estruturas/No.h"

std::vector<Face*> leitorObj(std::string arquivo);
std::vector<std::string*> split (std::string linha, char separador);
void exportar (std::string arquivo, No *arvore, float tam, Eigen::Vector3d centro, std::ofstream *arq = NULL);

#endif
