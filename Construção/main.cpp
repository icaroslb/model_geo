#include <iostream>
#include <GL/freeglut.h>
#include "Eigen/Geometry"

#include "modelo/Esfera.h"
#include "modelo/Bloco.h"
#include "leitor.h"

OcTree arvoreEsfera,
       arvoreBloco,
	   arvoreFaces,
	   arvoreUniao;
bool aramado = true;
int largura = 300, altura = 300;
double volume = 0;

std::vector<Face*> obj;

Eigen::Vector3d frente(0.0f, 0.0f, 1.0f),
                posi(0.0f, 0.0f, 0.0f),
				cima(0.0f, 1.0f, 0.0f),
				direita = frente.cross(cima),
				lookAt = frente;

void iniciar ();
void desenharCena ();
void resize (int w, int h);
void redesenharCena (int value);
void teclado (unsigned char key, int x, int y);

int main (int argc, char *argv[]) {
    glutInit(&argc, argv);

	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
	
	glutInitWindowSize(2*largura, 2*altura);
	glutInitWindowPosition(100, 100);

	glutTimerFunc(1000.0/60.0, redesenharCena, 0);
	
	glutCreateWindow("modelagem");
	
	glutDisplayFunc(desenharCena);
	glutReshapeFunc(resize);
	glutKeyboardFunc(teclado);

	iniciar();

	glutMainLoop();

    return 0;
}

void iniciar () {
	Esfera esfera(Eigen::Vector3d(0, 0, 0), 50);
	Bloco bloco(Eigen::Vector3d(0, 0, 0), 50, 20, 50);
	
	//arvoreEsfera = construir(&esfera, esfera.obterCentro(), esfera.obterLargura(), 8);
	std::thread constru(construirI, &esfera, std::ref(arvoreEsfera), esfera.obterCentro(), esfera.obterLargura(), 8, 0, nullptr);
	//std::thread constru2(construirI, &bloco, std::ref(arvoreBloco), Eigen::Vector3d(0, 0, 0), 50, 8, 0, nullptr);
	constru.join();
	//constru2.join();
	//construirI(&esfera, arvoreEsfera, esfera.obterCentro(), esfera.obterLargura(), 8);
	//construirI(&bloco, arvoreBloco, Eigen::Vector3d(0, 0, 0), 50, 8);
	
	//obj = leitorObj ("pinguim_low_poly.obj");
	//arvoreFaces = construir(&obj, Eigen::Vector3d(0.654088, 48.3725, 6.25491), 53.1872, 10, 0);
	//arvoreFaces = construir(&obj, Eigen::Vector3d(0, 0, 0), 10, 6, 0);
	
	//arvoreUniao.raiz = uniao(arvoreEsfera.raiz, arvoreBloco.raiz);
	//escalonar(arvoreUniao.raiz, 10, Eigen::Vector3d(0, 0, 0));

	exportar (std::string("teste"), arvoreUniao.raiz, 50, Eigen::Vector3d(0, 0, 0));

	glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
}

void desenharCena () {
	glClearColor(0.17, 0.17, 0.17, 1.00);

	glClear(GL_COLOR_BUFFER_BIT);
	glLoadIdentity();

	gluLookAt(posi.x(), posi.y(), posi.z(),
  		      lookAt.x(), lookAt.y(), lookAt.z(),
  		      cima.x(), cima.y(), cima.z());
	
	glColor3f(0.0, 0.80, 0.0);

	arvoreEsfera.mostrar();
	//arvoreBloco.mostrar();
	//arvoreUniao.mostrar();
	//mostrarFaces(obj);
	//arvoreFaces.mostrar();

	glFlush();
}

void resize (int w, int h) {
	glViewport(0, 0, w, h);

	largura = w/2;
	altura = h/2;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	//glOrtho(-25.0, 25.0, -25.0, 25.0, 0.0, 1.0);
	glFrustum(-1, 1, -1, 1, 1.0, 1000.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void redesenharCena (int value) {
	glutSwapBuffers();
	glutPostRedisplay();
	glutTimerFunc(1000.0/60.0, redesenharCena, 0);
}

void teclado (unsigned char key, int x, int y) {
	switch (key) {
	case 32:
		if (aramado) {
			glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
			aramado = false;
		} else {
			glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
			aramado = true;
		}
	break;

	case 'w':
		posi += frente;
		lookAt += frente;
	break;

	case 'a':
		posi -= direita;
		lookAt -= direita;
	break;

	case 's':
		posi -= frente;
		lookAt -= frente;
	break;

	case 'd':
		posi += direita;
		lookAt += direita;
	break;

	case 'v':
		std::cout << arvoreUniao.calcularVolume() << std::endl;
	break;
	default:
	break;
	}
}
