#include "Aresta.h"

Aresta::Aresta (int id, Vertice *inicio, Vertice *fim, Face *direita, Face *esquerda, Aresta *proxDir, Aresta *antDir, Aresta *proxEsq, Aresta *antEsq) :
id(id),
inicio(inicio),
fim(fim),
faceDireita(direita),
faceEsquerda(esquerda),
proxDir(proxDir),
antDir(antDir),
proxEsq(proxEsq),
antEsq(antEsq) {
    try { proxDir->inserirArestaOrdem(this); } catch (int erro) { }
    try { proxEsq->inserirArestaOrdem(this); } catch (int erro) { }
    try { antDir->inserirArestaOrdem(this); } catch (int erro) { }
    try { antEsq->inserirArestaOrdem(this); } catch (int erro) { }
}

void Aresta::inserirArestaOrdem (Aresta *aresta) {
    if (inicio == aresta->inicio) {
        if (faceDireita == aresta->faceDireita) {
            proxEsq = aresta;
        } else {
            proxDir = aresta;
        }
    } else if (inicio == aresta->fim) {
        if (faceDireita == aresta->faceDireita) {
            proxDir = aresta;
	    } else {
            proxEsq = aresta;
	    }
    } else if (fim == aresta->inicio) {
        if (faceDireita == aresta->faceDireita) {
            antDir = aresta;
	    } else {
            antEsq = aresta;
	    }
    } else {
        if (faceDireita == aresta->faceDireita) {
            antEsq = aresta;
	    } else {
            antDir = aresta;
	    }
    }
}

Aresta* Aresta::obterProximaDireita () {
    return proxDir;
}

Aresta* Aresta::obterProximaEsquerda () {
    return proxEsq;
}

Aresta* Aresta::obterAnteriorDireita () {
    return antDir;
}

Aresta* Aresta::obterAnteriorEsquerda () {
    return antEsq;
}

Face* Aresta::obterFaceDireita() {
    return faceDireita;
}

Face* Aresta::obterFaceEsquerda() {
    return faceEsquerda;
}

Vertice* Aresta::obterVerticeInicio() {
    return inicio;
}

Vertice* Aresta::obterVerticeFim() {
    return fim;
}