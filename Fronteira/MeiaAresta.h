#ifndef MEIA_ARESTA_H
#define MEIA_ARESTA_H

#include "Aresta.h"

class MeiaAresta {
    int idVertice,
        idFace;
    Aresta *aresta;
    MeiaAresta *anterior,
               *proximo;

public:
    MeiaAresta ();

    void inserirProximo (MeiaAresta *mAProx);
    void inserirAnterior (MeiaAresta *mAAnt);

    Aresta* obterAresta ();
    MeiaAresta* obterAnterior ();
    MeiaAresta* obterProximo ();
};

#endif