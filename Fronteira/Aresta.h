#ifndef ARESTA_H
#define ARESTA_H

#include "Vertice.h"
#include "Face.h"

class Aresta {
    int id;
    Vertice *inicio,
            *fim;
    Face *faceDireita,
         *faceEsquerda;
    Aresta *proxDir,
           *antDir,
           *proxEsq,
           *antEsq;

public:
    Aresta (int id, Vertice *inicio, Vertice *fim, Face *direita, Face *esquerda, Aresta *proxDir, Aresta *antDir, Aresta *proxEsq, Aresta *antEsq);
    void inserirArestaOrdem (Aresta *aresta);
    
    Aresta* obterProximaDireita ();
    Aresta* obterProximaEsquerda ();
    Aresta* obterAnteriorDireita ();
    Aresta* obterAnteriorEsquerda ();

    Face* obterFaceDireita();
    Face* obterFaceEsquerda();

    Vertice* obterVerticeInicio();
    Vertice* obterVerticeFim();
};

#endif
