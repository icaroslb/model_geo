#ifndef VERTICE_H
#define VERTICE_H

#include "Eigen/Dense"

class Vertice {
    int id,
        idAresta;
    Eigen::Vector4d coordenadas;

public:
    Vertice (int id, Eigen::Vector4d coordenadas, int idAresta = -1);

    void mudarIdAresta (int novoId);
    void mudarCoordenadas (Eigen::Vector4d novasCoordenadas);

    int obterId ();
    int obterIdAresta ();
    Eigen::Vector4d obterCoordenadas();

    void aplicarMatriz (Eigen::MatrixXd matriz);
};

#endif