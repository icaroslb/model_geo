#ifndef OBJETO_H
#define OBJETO_H

#include <vector>
#include "Aresta.h"
#include "Vertice.h"
#include "Face.h"

class Objeto {
    std::vector <Aresta*> arestas;
    std::vector <Vertice*> vertices;
    std::vector <Face*> faces;

public:
    Objeto ();

    void incluirAresta (Aresta *novaAresta);
    void incluirVertice (Vertice *novoVertice);
    void incluirFace (Face *novaFace);

    Aresta obterAresta (Aresta *novaAresta);
    Vertice obterVertice (Vertice *novoVertice);
    Face obterFace (Face *novaFace);

    int qtdArestas ();
    int qtdVertices ();
    int qtdFaces ();
};

#endif