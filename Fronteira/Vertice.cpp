#include "Vertice.h"

Vertice::Vertice (int id, Eigen::Vector4d coordenadas, int idAresta) :
id(id),
idAresta(idAresta),
coordenadas(coordenadas) { }

void Vertice::mudarIdAresta (int novoId) {
    idAresta = novoId;
}

void Vertice::mudarCoordenadas (Eigen::Vector4d novasCoordenadas) {
    coordenadas = novasCoordenadas;
}

int Vertice::obterId () {
    return id;
}

int Vertice::obterIdAresta () {
    return idAresta;
}

Eigen::Vector4d Vertice::obterCoordenadas() {
    return coordenadas;
}

void Vertice::aplicarMatriz (Eigen::MatrixXd matriz) {
    coordenadas = matriz * coordenadas;
}