#ifndef LEITOR_H
#define LEITOR_H

#include <fstream>
#include <string>
#include <list>
#include "Objeto.h"
#include "MeiaAresta.h"

struct Grafo {
    int no;
    MeiaAresta *mAresta;

    Grafo (int no, MeiaAresta *mAresta) : no(no), mAresta(mAresta) { }
    MeiaAresta* obterAresta ();
};

Objeto* leitorObj (std::string nomeArquivo);

#endif