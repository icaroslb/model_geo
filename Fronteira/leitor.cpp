#include "leitor.h"

Objeto* leitorObj (std::string nomeArquivo) {
    std::vector <MeiaAresta*> meiaArestas;
    std::vector <std::list <Grafo*>> grafo;
    std::string linha;
    Objeto *objeto = new Objeto();
    Grafo *no1,
          *no2;
    Aresta *novaAresta;

    std::fstream arquivo (nomeArquivo.data());

    while (std::getline(arquivo, linha)) {
        switch (linha[0]) {
        case 'v':
            objeto->incluirVertice(lerVertice(objeto->qtdVertices(), linha.substr(2, linha.size()-2)));
            grafo.push_back(std::list <Grafo*> {});
        break;
        case 'f':
            lerFace(linha.substr(2, linha.size()-2), grafo);
        break;
        default:
        break;
        }
    }

    arquivo.close();

    for (std::list <Grafo*> &lista : grafo) {
        lista.sort([] (Grafo* no1, Grafo* no2) { return no1->no < no2->no; });
    }

    for (int i = 0; i < grafo.size(); i++) {
        while (grafo[i].size() > 0) {
            no1 = grafo[i].front();
            grafo[i].pop_front();
            no2 = grafo[no1->no].front();
            grafo[no1->no].pop_front();

            //novaAresta = new Aresta(objeto->qtdArestas(), , , , , , , , );
        }
    }
}

Vertice* lerVertice (int idVertice, std::string linha) {
    Vertice *novoVertice;
    Eigen::Vector4d vCoordenadas;
    double coordenadas[3];

    for (int i = 0; i < 3; i++) {
        coordenadas[i] = std::stod(linha.substr(0, linha.find(' ')));
        linha.erase(0, linha.find(' ') + 1);
    }

    vCoordenadas.x() = coordenadas[0];
    vCoordenadas.y() = coordenadas[1];
    vCoordenadas.z() = coordenadas[2];
    vCoordenadas.w() = 0;

    novoVertice = new Vertice(idVertice, vCoordenadas);

    return novoVertice;
}

void lerFace (std::string linha, std::vector <std::list <Grafo*>> &grafo) {
    int idVertices[3];
    Grafo *novosNos[3];
    
    for (int i = 0; i < 3; i++) {
        idVertices[i] = std::stoi(linha.substr(0, linha.find(' '))) - 1;
        linha.erase(0, linha.find(' ') + 1);
    }

    for (int i = 0; i < 3; i++) {
        novosNos[i] = new Grafo(idVertices[(i+1)%3], new MeiaAresta());
    }

    for (int i = 0; i < 3; i++) {
        novosNos[i]->obterAresta()->inserirProximo((novosNos[(i+1)%3]->obterAresta()));
        novosNos[i]->obterAresta()->inserirAnterior((novosNos[(i+2)%3]->obterAresta()));
        grafo[idVertices[i]].push_back(novosNos[i]);
    }
}