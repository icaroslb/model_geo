#include <iostream>
#include <random>
#include <GL/freeglut.h>
#include "Eigen/Geometry"
#include "modelo/Objeto.h"
#include "primitivas/Circulo.h"
#include "primitivas/Retangulo.h"
#include "primitivas/Penguim.h"
#include "operadores/operadores.h"

Eigen::Vector3d frente(0.0f, 0.0f, -1.0f),
                posi(0.0f, 0.0f, 0.0f),
				cima(0.0f, 1.0f, 0.0f),
				direita = frente.cross(cima),
				lookAt = frente;
std::vector <Vertice*> vertices;
std::vector <Aresta*> arestas;
Aresta *aresta;
Face *face;
Objeto *obj;

void iniciar ();
void desenharCena ();
void resize (int w, int h);
void redesenharCena (int value);
void teclado (unsigned char key, int x, int y);

int main (int argc, char *argv[]) {
    glutInit(&argc, argv);

	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
	
	glutInitWindowSize(600, 600);
	glutInitWindowPosition(100, 100);

	glutTimerFunc(1000.0/60.0, redesenharCena, 0);
	
	glutCreateWindow("Fronteira - Icaro");
	
	glutDisplayFunc(desenharCena);
	glutReshapeFunc(resize);
	glutKeyboardFunc(teclado);

	iniciar();

	glutMainLoop();

    return 0;
}

void iniciar () {
	glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);

	std::random_device rd;
	for (int i = 0; i < 10; i++) {
		vertices.push_back(new Vertice(i, Eigen::Vector3d(((float)rd()/((float)rd.max()/2.0)) - 1, ((float)rd()/((float)rd.max()/2.0)) - 1, 1.0)));
	}

	//face = new Face(0, 0);
	//arestas.push_back(new Aresta(0, vertices[4], vertices[6], face, nullptr, nullptr, nullptr, nullptr, nullptr));
	//arestas.push_back(new Aresta(1, vertices[2], vertices[4], face, nullptr, nullptr, arestas[0], nullptr, nullptr));
	//arestas.push_back(new Aresta(2, vertices[6], vertices[2], face, nullptr, arestas[0], arestas[1], nullptr, nullptr));
	//obj = new Circulo(Eigen::Vector3d(0.0, 0.0, 0.0), 0.5, 13);
	//obj = new Retangulo(Eigen::Vector3d(0.0, 0.0, 0.0), 1, 1);
	obj = new Penguim();

	//obj->aplicarMatriz(escala(Eigen::Vector3d(0.0, 0.0, 0.0), 1, 0.4));
	//obj->aplicarMatriz(rotacionar(Eigen::Vector3d(0.0, 0.0, 1.0), 10));
}

void desenharCena () {
	glClearColor(0.17, 0.17, 0.17, 1.00);

	glClear(GL_COLOR_BUFFER_BIT);
	glLoadIdentity();

	gluLookAt(posi.x(), posi.y(), posi.z(),
  		      lookAt.x(), lookAt.y(), lookAt.z(),
  		      cima.x(), cima.y(), cima.z());

	obj->mostrar();
	
	glFlush();
}

void resize (int w, int h) {
	glViewport(0, 0, w, h);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-1, 1, -1, 1, -1.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void redesenharCena (int value) {
	glutSwapBuffers();
	glutPostRedisplay();
	glutTimerFunc(1000.0/60.0, redesenharCena, 0);
}

void teclado (unsigned char key, int x, int y) {
	float X = (x-300.0)/300.0,
	      Y = (300.0-y)/300.0;
	Eigen::Vector3d comp(X, Y, 1.0);

	switch (key) {
	case 'w':
		posi += frente;
		lookAt += frente;
	break;

	case 'a':
		posi -= direita;
		lookAt -= direita;
	break;

	case 's':
		posi -= frente;
		lookAt -= frente;
	break;

	case 'd':
		posi += direita;
		lookAt += direita;
	break;

	case 'c':
		std::cout << "\n" << obj->area() << "\n";
	break;

	default:
	break;
	}
}