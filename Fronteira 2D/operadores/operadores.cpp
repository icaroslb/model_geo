#include "operadores.h"

Eigen::MatrixXd escala (Eigen::Vector3d posicao, double x, double y) {
	Eigen::MatrixXd matriz(3, 3);

	matriz << x, 0, 0,
	          0, y, 0,
	          0, 0, 1;

	return mover(-posicao.x(), -posicao.y()) * matriz * mover(posicao.x(), posicao.y());
}

Eigen::MatrixXd mover (double x, double y) {
	Eigen::MatrixXd matriz(3, 3);

	matriz << 1, 0, x,
	          0, 1, y,
	          0, 0, 1;
	return matriz;
}

Eigen::MatrixXd rotacionar (Eigen::Vector3d posicao, double alfa) {
	Eigen::MatrixXd matriz(3, 3);

	matriz << cos(alfa), -sin(alfa), 0,
	          sin(alfa),  cos(alfa), 0,
	          0, 0, 1;

	return mover(-posicao.x(), -posicao.y()) * matriz * mover(posicao.x(), posicao.y());
}