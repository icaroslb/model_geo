#include "../Eigen/Dense"

Eigen::MatrixXd escala (Eigen::Vector3d posicao, double x, double y);
Eigen::MatrixXd mover (double x, double y);
Eigen::MatrixXd rotacionar (Eigen::Vector3d posicao, double alfa);