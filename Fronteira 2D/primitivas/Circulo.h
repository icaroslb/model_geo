#ifndef CIRCULO_H
#define CIRCULO_H

#include "../modelo/Objeto.h"

class Circulo : public Objeto {
public:
    Circulo (Eigen::Vector3d centro, double raio, int divisao);
};

#endif