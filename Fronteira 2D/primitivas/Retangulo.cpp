#include "Retangulo.h"

Retangulo::Retangulo (Eigen::Vector3d centro, double largura, double altura) {
    Eigen::Vector3d posicao;
    int arestasIds[3];

    posicao.x() -= largura/2.0;
    posicao.y() += altura/2.0;
    MVFS(*this, centro);

    MEV(*this, 1, 0, 0, posicao);

    posicao.y() -= altura;
	MEV(*this, 2, 1, 0, posicao);

    posicao.x() += largura;
	MEV(*this, 3, 2, 0, posicao);

	posicao.y() += altura;
	MEV(*this, 4, 3, 0, posicao);

	MEF(*this, 5, 1, 2, 1, FACE_DIREITA, 1, 0);
	MEF(*this, 6, 2, 3, 2, FACE_DIREITA, 2, 1);
	MEF(*this, 7, 3, 4, 3, FACE_DIREITA, 3, 2);
	MEF(*this, 8, 4, 1, 4, FACE_DIREITA, 3, 0);

	//MEF(*this, 3, 1, 3, 0, FACE_DIREITA, arestas.size(), arestasIds);
}