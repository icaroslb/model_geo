#ifndef RETANGULO_H
#define RETANGULO_H

#include "../modelo/Objeto.h"

class Retangulo : public Objeto {
public:
	Retangulo (Eigen::Vector3d centro, double largura, double altura);
};

#endif