#include "Circulo.h"
#include <iostream>
Circulo::Circulo (Eigen::Vector3d centro, double raio, int divisao) {
    Eigen::Vector3d movimento(0.0, 0.0, 0.0);
    double espaco = (2 * M_PI) / (double)divisao,
           atual = 0.0;

    movimento.x() = std::cos(atual) * raio;
    movimento.y() = std::sin(atual) * raio;
    MVFS(*this, centro);

    for (int i = 1; i < divisao+1; i++) {
        atual += espaco;
        movimento.x() = std::cos(atual) * raio;
        movimento.y() = std::sin(atual) * raio;

        
        MEV(*this, i, i-1, 0, centro + movimento);
    }

    for (int i = divisao+1; i < divisao*2; i++){
        MEF(*this, i, i-divisao, i-divisao, i-divisao+1, FACE_ESQUERDA, i-divisao-1, i-divisao);
    }

    MEF(*this, divisao*2, divisao, divisao, 1, FACE_ESQUERDA, 0, divisao-1);
}