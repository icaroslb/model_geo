#include "Penguim.h"

Penguim::Penguim () {
	Eigen::Vector3d posicao(0.0, 0.0, 0.0);

	MVFS(*this, posicao);
	
	posicao << 0.1, 0.1, 0;
	MEV(*this, 1, 0, 0, posicao);
	
	posicao << -0.1, 0.1, 0;
	MEV(*this, 2, 1, 0, posicao);
	
	posicao << -0.3, 0.05, 0;
	MEV(*this, 3, 2, 0, posicao);
	
	posicao << -0.3, -0.2, 0;
	MEV(*this, 4, 3, 0, posicao);
	
	posicao << 0.3, -0.2, 0;
	MEV(*this, 5, 4, 0, posicao);
	
	posicao << 0.3, 0.05, 0;
	MEV(*this, 6, 5, 0, posicao);

	MEF(*this, 7, 1, 1, 2, FACE_ESQUERDA, 1, 0);
	MEF(*this, 8, 2, 2, 3, FACE_ESQUERDA, 2, 1);
	MEF(*this, 9, 3, 3, 4, FACE_ESQUERDA, 3, 2);
	MEF(*this, 10, 4, 4, 5, FACE_ESQUERDA, 4, 3);
	MEF(*this, 11, 5, 5, 6, FACE_ESQUERDA, 5, 4);
	MEF(*this, 12, 6, 6, 1, FACE_ESQUERDA, 0, 5);

}