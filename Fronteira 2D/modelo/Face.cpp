#include "Face.h"

Face::Face (int id, int idAresta) : id(id), idAresta(idAresta) { }

int Face::obterId () {
    return id;
}

int Face::obterAresta () {
    return idAresta;
}

bool Face::operator == (int comp) {
    return id == comp;
}