#ifndef FACE_H
#define FACE_H

#include "../Eigen/Dense"

class Face {
    int id,
        idAresta;

public:
    Face (int id, int idAresta = -1);

    int obterId ();
    int obterAresta ();

    bool operator == (int comp);
    bool comparar (Eigen::Vector3d ponto);
};

#endif