#ifndef ARESTA_H
#define ARESTA_H

#include <exception>
#include "Vertice.h"
#include "Face.h"

enum {FACE_ESQUERDA, FACE_DIREITA};

class Aresta {
    int id;
    Vertice *inicio,
            *fim;
    Face *faceDireita,
         *faceEsquerda;
    Aresta *proxDir,
           *antDir,
           *proxEsq,
           *antEsq;

public:
    Aresta (int id, Vertice *inicio, Vertice *fim, Face *direita, Face *esquerda, Aresta *proxDir, Aresta *antDir, Aresta *proxEsq, Aresta *antEsq);
    void inserirArestaOrdem (Aresta *aresta);
    
    int obterId ();
    Aresta* obterProximaDireita ();
    Aresta* obterProximaEsquerda ();
    Aresta* obterAnteriorDireita ();
    Aresta* obterAnteriorEsquerda ();

    Face* obterFaceDireita();
    Face* obterFaceEsquerda();
    void inserirFaceDireita(Face* novaFace);
    void inserirFaceEsquerda(Face* novaFace);

    Vertice* obterVerticeInicio();
    Vertice* obterVerticeFim();

    bool operator== (int comp) { return id == comp; }
    bool comparar (Eigen::Vector3d ponto);
};

#endif
