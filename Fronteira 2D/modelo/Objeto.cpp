#include "Objeto.h"

Objeto::Objeto () : arestas(), vertices(), faces() { }

void Objeto::incluirAresta (Aresta *novaAresta) {
    arestas.push_back(novaAresta);
}

void Objeto::incluirVertice (Vertice *novoVertice) {
    vertices.push_back(novoVertice);
}

void Objeto::incluirFace (Face *novaFace) {
    faces.push_back(novaFace);
}

Aresta* Objeto::obterAresta (int arestaId) {
    std::vector<Aresta*>::iterator veri;

    for (veri = arestas.begin(); veri != arestas.end(); veri++) {
        if ((*veri)->obterId() == arestaId) {
            return *veri;
        }
    }
}

Aresta* Objeto::obterAresta (Eigen::Vector3d arestaPos) {
    for (Aresta *a :arestas) {

    }
}

Vertice* Objeto::obterVertice (int verticeId) {
    std::vector<Vertice*>::iterator veri;

    for (veri = vertices.begin(); veri != vertices.end(); veri++) {
        if ((*veri)->obterId() == verticeId) {
            return *veri;
        }
    }
}

Vertice* Objeto::obterVertice (Eigen::Vector3d verticePos) {
    for (Vertice *v : vertices) {
        
    }
}

Face* Objeto::obterFace (int faceId) {
    //return *(std::find(faces.begin(), faces.end(), faceId));
}

Face* Objeto::obterFace (Eigen::Vector3d facePos) {
    for (Face *f : faces) {

    }
}

int Objeto::qtdArestas () {
    return arestas.size();
}

int Objeto::qtdVertices () {
    return vertices.size();
}

int Objeto::qtdFaces () {
    return faces.size();
}

void Objeto::mostrar () {
    Aresta *aresta;
    int posicaoFace;

    glColor3f(1.0, 1.0, 1.0);
    glBegin(GL_TRIANGLES);
        for (int i = 1; i < faces.size(); i++) {
            aresta = obterAresta(faces[i]->obterAresta());
            if (aresta->obterFaceEsquerda() != nullptr && aresta->obterFaceEsquerda()->obterId() == faces[i]->obterId()) {
                glVertex2dv(aresta->obterVerticeInicio()->obterCoordenadas().data());
                aresta = aresta->obterProximaEsquerda();
                glVertex2dv(aresta->obterVerticeInicio()->obterCoordenadas().data());
                aresta = aresta->obterAnteriorDireita();
                glVertex2dv(aresta->obterVerticeInicio()->obterCoordenadas().data());
            } else {
                glVertex2dv(aresta->obterVerticeInicio()->obterCoordenadas().data());
                aresta = aresta->obterProximaDireita();
                glVertex2dv(aresta->obterVerticeInicio()->obterCoordenadas().data());
                aresta = aresta->obterAnteriorEsquerda();
                glVertex2dv(aresta->obterVerticeInicio()->obterCoordenadas().data());
            }
        }
    glEnd();

    glColor3f(1.0, 0.0, 0.0);
    glLineWidth(3.0);
    glBegin(GL_LINES);
        //glVertex2dv(arestas[0]->obterVerticeInicio()->obterCoordenadas().data());
        //glVertex2dv(arestas[0]->obterVerticeFim()->obterCoordenadas().data());
        for (Aresta *a : arestas) {
            glVertex2dv(a->obterVerticeInicio()->obterCoordenadas().data());
            glVertex2dv(a->obterVerticeFim()->obterCoordenadas().data());
        }
    glEnd();



    glColor3f(0.0, 1.0, 0.0);
    glPointSize(5.0);
    glBegin(GL_POINTS);
        for (Vertice *v : vertices) {
            glVertex2dv(v->obterCoordenadas().data());
        }
    glEnd();
}

void Objeto::aplicarMatriz (Eigen::MatrixXd matriz) {
    for (Vertice *v : vertices) {
        v->aplicarMatriz(matriz);
    }
}

double Objeto::area () {
    Aresta *aresta1,
           *aresta2;
    double valorArea = 0.0;

    for (int i = 1; i < faces.size(); i++) {
        aresta1 = obterAresta(faces[i]->obterAresta());

        if (aresta1->obterFaceEsquerda() != nullptr && aresta1->obterFaceEsquerda()->obterId() == faces[i]->obterId()) {
            aresta2 = aresta1->obterProximaEsquerda();
        } else {
            aresta2 = aresta1->obterProximaDireita();
        }
        
        valorArea += ((aresta1->obterVerticeInicio()->obterCoordenadas() - aresta1->obterVerticeFim()->obterCoordenadas()).cross(aresta2->obterVerticeFim()->obterCoordenadas() - aresta2->obterVerticeInicio()->obterCoordenadas())).norm();
    }

    return valorArea/2;
}

void MVFS (Objeto &obj, Eigen::Vector3d posicao) {
    Vertice *novoVertice = new Vertice(0, posicao);
    Face *novaFace = new Face(0);

    obj.incluirVertice(novoVertice);
    obj.incluirFace(novaFace);
}

void MEV (Objeto &obj, int idVertice, int idAresta, int idVerticeInicial, Eigen::Vector3d posicao) {
    Vertice *novoVertice = new Vertice(idVertice, posicao, idAresta);
    obj.incluirVertice(novoVertice);

    Aresta *novaAresta = new Aresta(idAresta, obj.obterVertice(idVerticeInicial), novoVertice, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr);
    obj.incluirAresta(novaAresta);
}

void MEF (Objeto &obj, int idAresta, int idFace, int idVerticeInicial, int idVerticeFinal, int posicaoFace, int arestaProxima, int arestaAnterior) {
    Face *novaFace = new Face(idFace, idAresta);

    Aresta *novaAresta,
           *aresta1 = obj.obterAresta(arestaProxima),
           *aresta2 = obj.obterAresta(arestaAnterior);
    
    if (posicaoFace == FACE_DIREITA) {
        novaAresta = new Aresta(idAresta, obj.obterVertice(idVerticeInicial), obj.obterVertice(idVerticeFinal), novaFace, nullptr, nullptr, nullptr, nullptr, nullptr);
    } else {
        novaAresta = new Aresta(idAresta, obj.obterVertice(idVerticeInicial), obj.obterVertice(idVerticeFinal), nullptr, novaFace, nullptr, nullptr, nullptr, nullptr);
    }

    if ((novaAresta->obterVerticeFim()->obterId() == aresta1->obterVerticeInicio()->obterId()) ||
        (novaAresta->obterVerticeInicio()->obterId() == aresta1->obterVerticeFim()->obterId())) {
        if (posicaoFace == FACE_DIREITA) {
            aresta1->inserirFaceDireita(novaFace);
        } else {
            aresta1->inserirFaceEsquerda(novaFace);
        }
    } else {
        if (posicaoFace == FACE_ESQUERDA) {
            aresta1->inserirFaceDireita(novaFace);
        } else {
            aresta1->inserirFaceEsquerda(novaFace);
        }
    }

    if ((novaAresta->obterVerticeFim()->obterId() == aresta2->obterVerticeInicio()->obterId()) ||
        (novaAresta->obterVerticeInicio()->obterId() == aresta2->obterVerticeFim()->obterId())) {
        if (posicaoFace == FACE_DIREITA) {
            aresta2->inserirFaceDireita(novaFace);
        } else {
            aresta2->inserirFaceEsquerda(novaFace);
        }
    } else {
        if (posicaoFace == FACE_ESQUERDA) {
            aresta2->inserirFaceDireita(novaFace);
        } else {
            aresta2->inserirFaceEsquerda(novaFace);
        }
    }

    aresta1->inserirArestaOrdem(aresta2);
    aresta2->inserirArestaOrdem(aresta1);
    aresta1->inserirArestaOrdem(novaAresta);
    aresta2->inserirArestaOrdem(novaAresta);
    novaAresta->inserirArestaOrdem(aresta2);
    novaAresta->inserirArestaOrdem(aresta1);

    obj.incluirFace(novaFace);
    obj.incluirAresta(novaAresta);
}