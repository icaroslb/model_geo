#ifndef OBJETO_H
#define OBJETO_H

#include <GL/gl.h>
#include <algorithm>
#include <vector>
#include <stdarg.h>
#include "Aresta.h"
#include "Vertice.h"
#include "Face.h"

class Objeto {
protected:
    std::vector <Aresta*> arestas;
    std::vector <Vertice*> vertices;
    std::vector <Face*> faces;

public:
    Objeto ();

    void incluirAresta (Aresta *novaAresta);
    void incluirVertice (Vertice *novoVertice);
    void incluirFace (Face *novaFace);

    Aresta* obterAresta (int arestaId);
    Aresta* obterAresta (Eigen::Vector3d arestaPos);
    Vertice* obterVertice (int verticeId);
    Vertice* obterVertice (Eigen::Vector3d verticePos);
    Face* obterFace (int faceId);
    Face* obterFace (Eigen::Vector3d facePos);

    int qtdArestas ();
    int qtdVertices ();
    int qtdFaces ();

    void mostrar ();
    void aplicarMatriz (Eigen::MatrixXd matriz);

    double area ();
};

void MVFS (Objeto &obj, Eigen::Vector3d posicao);
void MEV (Objeto &obj, int idVertice, int idAresta, int idVerticeInicial, Eigen::Vector3d posicao);
void MEF (Objeto &obj, int idAresta, int idFace, int idVerticeInicial, int idVerticeFinal, int posicaoFace, int arestaProxima, int arestaAnterior);

#endif