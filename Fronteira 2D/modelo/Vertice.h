#ifndef VERTICE_H
#define VERTICE_H

#include "../Eigen/Dense"

class Vertice {
    int id,
        idAresta;
    Eigen::Vector3d coordenadas;

public:
    Vertice (int id, Eigen::Vector3d coordenadas, int idAresta = -1);

    void mudarIdAresta (int novoId);
    void mudarCoordenadas (Eigen::Vector3d novasCoordenadas);

    int obterId ();
    int obterIdAresta ();
    Eigen::Vector3d obterCoordenadas();

    void aplicarMatriz (Eigen::MatrixXd matriz);

    bool operator == (const int &comp) const { return id == comp; }
    bool comparar (Eigen::Vector3d ponto);
};

#endif