#include "Aresta.h"

Aresta::Aresta (int id, Vertice *inicio, Vertice *fim, Face *direita, Face *esquerda, Aresta *proxDir, Aresta *antDir, Aresta *proxEsq, Aresta *antEsq) :
id(id),
inicio(inicio),
fim(fim),
faceDireita(direita),
faceEsquerda(esquerda),
proxDir(proxDir),
antDir(antDir),
proxEsq(proxEsq),
antEsq(antEsq) {
    if (proxDir != nullptr)
        proxDir->inserirArestaOrdem(this);
    if (proxEsq != nullptr)
        proxEsq->inserirArestaOrdem(this);
    if (antDir != nullptr)
        antDir->inserirArestaOrdem(this);
    if (antEsq != nullptr)
        antEsq->inserirArestaOrdem(this);
}

void Aresta::inserirArestaOrdem (Aresta *aresta) {
    if (inicio == aresta->inicio) {
        if (faceEsquerda != nullptr && aresta->faceDireita != nullptr && faceEsquerda->obterId() == aresta->faceDireita->obterId()) {
            proxEsq = aresta;
        } else {
            proxDir = aresta;
        }
    } else if (inicio == aresta->fim) {
        if (faceDireita != nullptr && aresta->faceDireita != nullptr && faceDireita->obterId() == aresta->faceDireita->obterId()) {
            proxDir = aresta;
	    } else {
            proxEsq = aresta;
	    }
    } else if (fim == aresta->inicio) {
        if (faceDireita != nullptr && aresta->faceDireita != nullptr && faceDireita->obterId() == aresta->faceDireita->obterId()) {
            antDir = aresta;
	    } else {
            antEsq = aresta;
	    }
    } else {
        if (faceEsquerda != nullptr && aresta->faceDireita != nullptr && faceEsquerda->obterId() == aresta->faceDireita->obterId()) {
            antEsq = aresta;
	    } else {
            antDir = aresta;
	    }
    }
}

int Aresta::obterId () {
    return id;
}

Aresta* Aresta::obterProximaDireita () {
    return proxDir;
}

Aresta* Aresta::obterProximaEsquerda () {
    return proxEsq;
}

Aresta* Aresta::obterAnteriorDireita () {
    return antDir;
}

Aresta* Aresta::obterAnteriorEsquerda () {
    return antEsq;
}

Face* Aresta::obterFaceDireita() {
    return faceDireita;
}

Face* Aresta::obterFaceEsquerda() {
    return faceEsquerda;
}

void Aresta::inserirFaceDireita(Face* novaFace) {
    faceDireita = novaFace;
}

void Aresta::inserirFaceEsquerda(Face* novaFace) {
    faceEsquerda = novaFace;
}
Vertice* Aresta::obterVerticeInicio() {
    return inicio;
}

Vertice* Aresta::obterVerticeFim() {
    return fim;
}

bool Aresta::comparar (Eigen::Vector3d ponto) {

}