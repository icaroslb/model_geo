#include "Vertice.h"

Vertice::Vertice (int id, Eigen::Vector3d coordenadas, int idAresta) :
id(id),
idAresta(idAresta),
coordenadas(coordenadas) { }

void Vertice::mudarIdAresta (int novoId) {
    idAresta = novoId;
}

void Vertice::mudarCoordenadas (Eigen::Vector3d novasCoordenadas) {
    coordenadas = novasCoordenadas;
}

int Vertice::obterId () {
    return id;
}

int Vertice::obterIdAresta () {
    return idAresta;
}

Eigen::Vector3d Vertice::obterCoordenadas() {
    return coordenadas;
}

void Vertice::aplicarMatriz (Eigen::MatrixXd matriz) {
    coordenadas = matriz * coordenadas;
}

bool Vertice::comparar (Eigen::Vector3d ponto) {
    return ((coordenadas - ponto).norm() < 0.02);
}