#include <iostream>
#include <GL/freeglut.h>
#include "Eigen/Geometry"
#include <random>

#include "visao/desenhar.h"

std::random_device rd;
int largura = 450, altura = 450;

Eigen::Vector3d frente(0.0f, 0.0f, -1.0f),
                posi(0.0f, 0.0f, 0.0f),
				cima(0.0f, 1.0f, 0.0f),
				direita = frente.cross(cima),
				lookAt = frente;

std::vector<No*> objetos;

void iniciar ();
void gerarCena ();
void gerarPenguim ();
void desenharCena ();
void resize (int w, int h);
void redesenharCena (int value);
void teclado (unsigned char key, int x, int y);

int main (int argc, char *argv[]) {
    glutInit(&argc, argv);

	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
	
	glutInitWindowSize(largura, altura);
	glutInitWindowPosition(100, 100);

	glutTimerFunc(1000.0/60.0, redesenharCena, 0);
	
	glutCreateWindow("CSG - Ícaro");
	
	glutDisplayFunc(desenharCena);
	glutReshapeFunc(resize);
	glutKeyboardFunc(teclado);

	iniciar();

	glutMainLoop();

    return 0;
}

void iniciar () {
	gerarCena();
	inicializarTela(2, 2, altura, largura);
	glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
}

void gerarCena () {
	No *no1 = new Esfera(Eigen::Vector3d(0, 0, 25), 10, Eigen::Vector3d(0.0, 1.0, 0.0)),
	   *no2 = new Bloco(Eigen::Vector3d(0, 0, 25), 15.0, 15.0, 15.0, Eigen::Vector3d(0.0, 1.0, 0.0));
	//objetos.push_back(no1);
	//objetos.push_back(no2);
	objetos.push_back(new Arvore(UNIAO, no1, no2, Eigen::Vector3d(0.0, 1.0, 0.0)));
	//objetos.push_back(no1);
	//objetos.push_back(no2);

	//gerarPenguim();
}

void gerarPenguim () {
	No *cabeca = new Esfera(Eigen::Vector3d(0, 10, 25), 5, Eigen::Vector3d(0.0, 1.0, 0.0)),
	   *bico = new Bloco(Eigen::Vector3d(0, 10, 20), 2.0, 2.0, 5.0, Eigen::Vector3d(0.0, 1.0, 0.0)),
	   *corpo = new Bloco(Eigen::Vector3d(0, 0, 25), 11.0, 20.0, 5.0, Eigen::Vector3d(0.0, 1.0, 0.0)),
	   *asaEsq = new Bloco(Eigen::Vector3d(-5.5, 4, 25), 5.0, 8.5, 2.0, Eigen::Vector3d(0.0, 1.0, 0.0)),
	   *asaDir = new Bloco(Eigen::Vector3d(5.5, 4, 25), 5.0, 8.5, 2.0, Eigen::Vector3d(0.0, 1.0, 0.0)),
	   *peEsq = new Bloco(Eigen::Vector3d(-3, -10, 25), 5.0, 4, 2.0, Eigen::Vector3d(0.0, 1.0, 0.0)),
	   *peDir = new Bloco(Eigen::Vector3d(3, -10, 25), 5.0, 4, 2.0, Eigen::Vector3d(0.0, 1.0, 0.0)),
	   *pes = new Arvore(UNIAO, peEsq, peDir, Eigen::Vector3d(0.0, 1.0, 0.0)),
	   *asas = new Arvore(UNIAO, asaEsq, asaDir, Eigen::Vector3d(0.0, 1.0, 0.0)),
	   *pesAsas = new Arvore(UNIAO, pes, asas, Eigen::Vector3d(0.0, 1.0, 0.0)),
	   *cabecaBico = new Arvore(UNIAO, cabeca, bico, Eigen::Vector3d(0.0, 1.0, 0.0)),
	   *cabecaBicoCorpo = new Arvore(UNIAO, cabecaBico, corpo, Eigen::Vector3d(0.0, 1.0, 0.0));

	   objetos.push_back(new Arvore(UNIAO, cabecaBicoCorpo, pesAsas, Eigen::Vector3d(0.0, 1.0, 0.0)));
}

void desenharCena () {
	glClearColor(0.17, 0.17, 0.17, 1.00);

	glClear(GL_COLOR_BUFFER_BIT);
	glLoadIdentity();

	gluLookAt(posi.x(), posi.y(), posi.z(),
  		      lookAt.x(), lookAt.y(), lookAt.z(),
  		      cima.x(), cima.y(), cima.z());
	
	glFlush();
	desenhar(altura, largura, std::ref(objetos));
}

void resize (int w, int h) {
	glViewport(0, 0, w, h);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-1, 1, -1, 1, -1.0, 1.0);
	//glFrustum(-1, 1, -1, 1, 1.0, 1000.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void redesenharCena (int value) {
	glutSwapBuffers();
	glutPostRedisplay();
	glutTimerFunc(1000.0/60.0, redesenharCena, 0);
}

void teclado (unsigned char key, int x, int y) {
	double crescer = (rd() / rd.max()) + (rd() % 2) + (0.5);
	std::vector<PontosInter> SMC;

	switch (key) {
	case 'w':
		posi += frente;
		lookAt += frente;
	break;

	case 'a':
		posi -= direita;
		lookAt -= direita;
	break;

	case 's':
		posi -= frente;
		lookAt -= frente;
	break;

	case 'd':
		posi += direita;
		lookAt += direita;
	break;

	case 'c':
		try {
			SMC = objetos[0]->smc(Eigen::Vector3d(0.0, 0.0, 1.0).normalized());

			for (PontosInter &smc : SMC){
				std::cout << smc;
			}
			std::cout << std::endl;
		} catch (int erro) {
			std::cout << "Não intercede\n";
		}
	break;
	case 'm':
		objetos[0]->mover(Eigen::Vector3d((int)(rd() % 30) - 15, (int)(rd() % 30) - 15, (int)(rd() % 30) - 15));
	break;

	case 'v':
		objetos[0]->escalonar(crescer, crescer, crescer);
	break;
	
	case 'x':
		std::cout << volume(obterPixels(), objetos[0], obterArea()) << std::endl;
	break;

	case 'e':
		std::cout << objetos[0]->volume() << std::endl;
	break;
	default:
	break;
	}
}
