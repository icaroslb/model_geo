#include "Cilindro.h"

Cilindro::Cilindro (Eigen::Vector3d centro, double raio, double altura, Eigen::Vector3d cor) :
centro(centro),
raio(raio),
altura (altura),
cor(cor) {
    Eigen::Vector3d vAltura(0.0, altura / 2, 0.0);
    ponta1 = centro + vAltura;
    ponta2 = centro - vAltura;
    vAlturaU = (ponta1 - ponta2).normalized();
}

Intersec Cilindro::intersecao (Eigen::Vector3d vetor) {
    Eigen::Vector3d w = -ponta1,
                    P;
}

Eigen::Vector3d& Cilindro::obterCor () {
    return cor;
}

Eigen::Vector3d Cilindro::obterNormal (Eigen::Vector3d ponto) {

}

std::vector<PontosInter> Cilindro::smc (Eigen::Vector3d ponto) {

}

void Cilindro::mover (Eigen::Vector3d movimento) {
    ponta1 += movimento;
    ponta2 += movimento;
    centro += movimento;
}

void Cilindro::escalonar (double x, double y, double z) {
    raio *= (x > z) ? x : z;
    altura *= y;
    ponta1 = ((ponta1 - centro) * y) + centro;
    ponta2 = ((ponta2 - centro) * y) + centro;
}

double Cilindro::volume () {
    return (M_PI * raio * raio) * altura;
}