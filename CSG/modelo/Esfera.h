#ifndef ESFERA_H
#define ESFERA_H

#include "No.h"

double delta (double a, double b, double c);

class Esfera : public virtual No {
private:
    double raio;
    Eigen::Vector3d centro,
                    cor;

public:
    Esfera (Eigen::Vector3d centro, double raio, Eigen::Vector3d cor);

    double& obterRaio ();
    Eigen::Vector3d& obterCentro ();

    Intersec intersecao (Eigen::Vector3d vetor);
    Eigen::Vector3d& obterCor ();
    Eigen::Vector3d obterNormal (Eigen::Vector3d ponto);
    std::vector<PontosInter> smc (Eigen::Vector3d vetor);

    void mover (Eigen::Vector3d movimento);
    void escalonar (double x, double y, double z);

    double volume ();
};

#endif