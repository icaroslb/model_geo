#include "Bloco.h"

Bloco::Bloco (Eigen::Vector3d centro, double largura, double altura, double profundidade, Eigen::Vector3d cor) : 
largura(largura),
altura(altura),
profundidade(profundidade),
centro(centro),
cor(cor)
{
    Eigen::Vector3d v1(largura / 2, 0.0, 0.0),
                    v2(0.0, altura / 2, 0.0),
                    v3(0.0, 0.0, profundidade / 2);
    p1 = centro + v1 + v2 - v3;
    p2 = centro - v1 + v2 - v3;
    p3 = centro - v1 - v2 - v3;
    p4 = centro + v1 - v2 - v3;
    p5 = centro + v1 + v2 + v3;
    p6 = centro - v1 + v2 + v3;
    p7 = centro - v1 - v2 + v3;
    p8 = centro + v1 - v2 + v3;
}

double& Bloco::obterLargura () {
    return largura;
}

double& Bloco::obterAltura () {
    return altura;
}

double& Bloco::obterProfundidade () {
    return profundidade;
}

double& Bloco::obterMaiorDimensao () {
    return (largura > altura && largura > profundidade) ? largura : (altura > profundidade) ? altura : profundidade;
}

Eigen::Vector3d& Bloco::obterCentro () {
    return centro;
}

Intersec Bloco::intersecao (Eigen::Vector3d vetor) {
    double dist;
    int idFace;
    bool viu = false;
    Intersec intersec;
    Eigen::Vector3d pf1,
                    pf2,
                    pf3,
                    pf4,
                    normal,
                    vTeste1,
                    vTeste2,
                    vTeste3,
                    vTeste4,
                    p;

    intersec.dist = INFINITY;
    for (int i = 0; i < 6; i++) {
        obterFace (i, pf1, pf2, pf3, pf4);
        normal = obterNormal(i);
        dist = pf1.dot(normal) / vetor.dot(normal);

        if (dist > 0 && intersec.dist > dist) {
            p = ((vetor * dist) - centro).cwiseAbs();
            if ((p.x() <= (largura / 2) + 0.00001f) &&
                (p.y() <= (altura / 2) + 0.00001f) &&
                (p.z() <= (profundidade / 2) + 0.00001f)) {
                    intersec.dist = dist;
                    intersec.normal = normal;
                    //intersec.normal = obterNormal(vetor * dist).normalized();
                    viu = true;
            }
        }
    }

    if (viu) {
        return intersec;
    } else {
        throw(SEM_INTERCESSAO);
    }
}

Eigen::Vector3d& Bloco::obterCor () {
    return cor;
}

Eigen::Vector3d Bloco::obterNormal (Eigen::Vector3d ponto) {
    return ponto - centro;
}

Eigen::Vector3d Bloco::obterNormal (int idFace) {
    switch (idFace) {
        case 0:
            return Eigen::Vector3d(0.0, 0.0, -1.0);
        break;
        case 1:
            return Eigen::Vector3d(-1.0, 0.0, 0.0);
        break;
        case 2:
            return Eigen::Vector3d(0.0, -1.0, 0.0);
        break;
        case 3:
            return Eigen::Vector3d(1.0, 0.0, 0.0);
        break;
        case 4:
            return Eigen::Vector3d(0.0, 0.0, 1.0);
        break;
        case 5:
            return Eigen::Vector3d(0.0, 1.0, 0.0);
        break;
    }
}

std::vector<PontosInter> Bloco::smc (Eigen::Vector3d vetor) {
    std::vector<PontosInter> intersecoes;
    Eigen::Vector3d pf1,
                    pf2,
                    pf3,
                    pf4,
                    normal,
                    vTeste1,
                    vTeste2,
                    vTeste3,
                    vTeste4,
                    p;
    double dist,
           inicio = -1,
           fim = -1,
           aux;
    bool achouUm = false;

    for (int i = 0; i < 6; i++) {
        obterFace (i, pf1, pf2, pf3, pf4);
        normal = obterNormal(i);
        dist = pf1.dot(normal) / vetor.dot(normal);

        if (dist > 0) {
            p = (vetor * dist);
            vTeste1 = (p - pf1).cross(p - pf2);
            vTeste2 = (p - pf2).cross(p - pf3);
            vTeste3 = (p - pf3).cross(p - pf4);
            vTeste4 = (p - pf4).cross(p - pf1);

            if ((vTeste1.dot(vTeste2) >= 0 && vTeste2.dot(vTeste3) >= 0 &&
                 vTeste3.dot(vTeste4) >= 0 && vTeste3.dot(vTeste1) >= 0) ||
                (vTeste1.dot(vTeste2) <= 0 && vTeste2.dot(vTeste3) <= 0 &&
                 vTeste3.dot(vTeste4) <= 0 && vTeste3.dot(vTeste1) <= 0)) {
                    if (!achouUm) {
                        inicio = dist;
                        achouUm = true;
                    } else {
                        fim = dist;
                        break;
                    }
            }
        }
    }

    if (inicio > fim) {
        aux = inicio;
        inicio = fim;
        fim = inicio;
    }

    if (inicio > 0.0 && fim > 0.0) {
        intersecoes.push_back(PontosInter(inicio, fim));
    } else if (fim > 0.0) {
        intersecoes.push_back(PontosInter(0.0, fim));
    } else {
        throw(SEM_INTERCESSAO);
    }

    return intersecoes;
}

void Bloco::mover (Eigen::Vector3d movimento) {
    centro += movimento;
    p1 += movimento;
    p2 += movimento;
    p3 += movimento;
    p4 += movimento;
    p5 += movimento;
    p6 += movimento;
    p7 += movimento;
    p8 += movimento;
}
#include <iostream>
void Bloco::escalonar (double x, double y, double z) {
    Eigen::MatrixXd escalar(3, 3);

    escalar << x, 0, 0,
               0, y, 0,
               0, 0, z;
    
    p1 = p1 - centro;
    p1 = escalar * p1;
    p1 = p1 + centro;

    p2 = p2 - centro;
    p2 = escalar * p2;
    p2 = p2 + centro;

    p3 = p3 - centro;
    p3 = escalar * p3;
    p3 = p3 + centro;

    p4 = p4 - centro;
    p4 = escalar * p4;
    p4 = p4 + centro;

    p5 = p5 - centro;
    p5 = escalar * p5;
    p5 = p5 + centro;

    p6 = p6 - centro;
    p6 = escalar * p6;
    p6 = p6 + centro;

    p7 = p7 - centro;
    p7 = escalar * p7;
    p7 = p7 + centro;
    
    p8 = p8 - centro;
    p8 = escalar * p8;
    p8 = p8 + centro;

    largura *= x; 
    altura *= y;
    profundidade *= z;
}

double Bloco::volume () {
    return largura * altura * profundidade;
}

void Bloco::obterFace (int idFace, Eigen::Vector3d &pf1, Eigen::Vector3d &pf2, Eigen::Vector3d &pf3, Eigen::Vector3d &pf4) {
    switch (idFace) {
        case 0:
            pf1 = p1;
            pf2 = p2;
            pf3 = p3;
            pf4 = p4;
        break;
        case 1:
            pf1 = p2;
            pf2 = p6;
            pf3 = p7;
            pf4 = p3;
        break;
        case 2:
            pf1 = p4;
            pf2 = p3;
            pf3 = p7;
            pf4 = p8;
        break;
        case 3:
            pf1 = p5;
            pf2 = p1;
            pf3 = p4;
            pf4 = p8;
        break;
        case 4:
            pf1 = p6;
            pf2 = p5;
            pf3 = p8;
            pf4 = p7;
        break;
        case 5:
            pf1 = p2;
            pf2 = p1;
            pf3 = p5;
            pf4 = p6;
        break;
    }
}