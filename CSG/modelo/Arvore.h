#ifndef ARVORE_H
#define ARVORE_H

#include "No.h"

enum {UNIAO, INTERSECAO, DIFERENCA};
enum {ERRO_ARVORE_NORMAL};

class Arvore : public virtual No {
private:
    int tipo;
    No *filho1,
       *filho2;
    Eigen::Vector3d cor;

public:
    Arvore (int tipo, No *filho1, No *filho2, Eigen::Vector3d cor);
    Intersec intersecao (Eigen::Vector3d vetor);
    Eigen::Vector3d& obterCor ();
    Eigen::Vector3d obterNormal (Eigen::Vector3d ponto);
    std::vector<PontosInter> smc (Eigen::Vector3d ponto);

    void mover (Eigen::Vector3d movimento);
    void escalonar (double x, double y, double z);

    double volume ();
};

#endif