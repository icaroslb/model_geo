#include "Esfera.h"

Esfera::Esfera (Eigen::Vector3d centro, double raio, Eigen::Vector3d cor){
    this->centro = centro;
    this->raio = raio;
    this->cor = cor;
}

double& Esfera::obterRaio () {
    return raio;
}

Eigen::Vector3d& Esfera::obterCentro () {
    return centro;
}

Intersec Esfera::intersecao (Eigen::Vector3d vetor) {
    Eigen::Vector3d w = (-centro);
    Intersec intersec;

    vetor;

    double a = vetor.dot(vetor),
           b = 2.0 * (vetor.dot(w)),
           c = w.dot(w) - pow(raio, 2),
           delta = pow(b, 2) - (4.0 * a * c),
           x1,
           x2;
    
    if (delta < 0) {
        throw(SEM_INTERCESSAO);
    } else if (delta == 0) {
        intersec.dist = (-b + sqrt(delta)) / (2.0 * a);
    } else {
        x1 = (-b + sqrt(delta)) / (2.0 * a);
        x2 = (-b - sqrt(delta)) / (2.0 * a);

        if (x1 < x2) {
            intersec.dist = (x1 >= 0) ? x1 : x2;
        } else {
            intersec.dist = (x2 >= 0) ? x2 : x1;
        }
    }

    intersec.normal = obterNormal((vetor * intersec.dist));

    return intersec;
}

Eigen::Vector3d Esfera::obterNormal (Eigen::Vector3d ponto) {
    return (ponto - centro).normalized();
}

Eigen::Vector3d& Esfera::obterCor () {
    return cor;
}

std::vector<PontosInter> Esfera::smc (Eigen::Vector3d vetor) {
    std::vector<PontosInter> intersecoes;

    Eigen::Vector3d w = -centro;
    double a = vetor.dot(vetor),
           b = 2.0 * (vetor.dot(w)),
           c = w.dot(w) - pow(raio, 2),
           delta = pow(b, 2) - (4.0 * a * c),
           x1,
           x2;
    
    if (delta < 0) {
        throw(SEM_INTERCESSAO);
    } else if (delta == 0) {
        x1 = (-b + sqrt(delta)) / (2.0 * a);
        
        if (x1 > 0) {
            intersecoes.push_back(PontosInter(x1, x1));
        } else {
            throw(SEM_INTERCESSAO);
        }
    } else {
        x1 = (-b - sqrt(delta)) / (2.0 * a);
        x2 = (-b + sqrt(delta)) / (2.0 * a);
        
        if (x1 > 0.0 && x2 > 0.0) {
            intersecoes.push_back(PontosInter(x1, x2));
        } else if (x2 > 0.0) {
            intersecoes.push_back(PontosInter(0.0, x2));
        } else {
            throw(SEM_INTERCESSAO);
        }
    }
    
    return intersecoes;
}

void Esfera::mover (Eigen::Vector3d movimento) {
    centro = centro + movimento;
}

void Esfera::escalonar (double x, double y, double z) {
    double maior = (x > y) ? ((x > z) ? x : z) : ((y > z) ? y : z);

    raio *= maior;
}

double Esfera::volume () {
    return (4 * M_PI * pow(raio, 3)) / 3;
}