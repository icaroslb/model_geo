#ifndef NO_H
#define NO_H

#include <cmath>
#include <vector>
#include <algorithm>
#include "../Eigen/Dense"

#define SEM_INTERCESSAO -1

enum { ENT_SAID, TOQUE };
enum { IGUAIS, ENTRE_1_DENTRO, ENTRE_2_DENTRO, ENTRE_DISJUNTOS_1_MENOR, ENTRE_DISJUNTOS_2_MENOR, DISJUNTOS_1_MENOR, DISJUNTOS_2_MENOR };
enum { NAO_INTERCEDE };

struct PontosInter {
    double inicio,
           fim;

    PontosInter (double inicio, double fim) : inicio(inicio), fim(fim) {}
    friend std::ostream& operator << (std::ostream& os, const PontosInter &pi) {
        os << pi.inicio << " --- " << pi.fim << ' ';
        return os;
    }
};

struct Intersec {
    double dist;
    Eigen::Vector3d normal;

    void operator = (Intersec copia) {
        dist = copia.dist;
        normal = copia.normal;
    }
};

struct CorPosicao {
    Eigen::Vector3d cor,
                    posicao;
};

class No {
public:
    virtual Intersec intersecao (Eigen::Vector3d vetor) = 0;
    virtual Eigen::Vector3d& obterCor () = 0;
    virtual Eigen::Vector3d obterNormal (Eigen::Vector3d ponto) = 0;
    virtual std::vector<PontosInter> smc (Eigen::Vector3d ponto) = 0;
    virtual void mover (Eigen::Vector3d movimento) = 0;
    virtual void escalonar (double x, double y, double z) = 0;

    virtual double volume () = 0;
};

static int analisarPontosIntersecao (PontosInter ps1, PontosInter ps2) {
    if (abs(ps1.inicio - ps2.inicio) < 0.001 && abs(ps1.fim - ps2.fim) < 0.001) {
        return IGUAIS;
    } else if (ps1.inicio < ps2.inicio && ps1.fim > ps2.fim) {
        return ENTRE_2_DENTRO;
    } else if (ps2.inicio < ps1.inicio && ps2.fim > ps1.fim) {
        return ENTRE_1_DENTRO;
    } else if (ps1.inicio < ps2.inicio && ps1.fim < ps2.fim) {
        return ENTRE_DISJUNTOS_1_MENOR;
    } else if (ps2.inicio < ps1.inicio && ps2.fim < ps1.fim) {
        return ENTRE_DISJUNTOS_2_MENOR;
    } else if (ps1.fim < ps2.inicio) {
        return DISJUNTOS_1_MENOR;
    } else {
        return DISJUNTOS_2_MENOR;
    }
}
#include <iostream>
static double volume (std::vector <CorPosicao> &telaPixels, No *no, double area) {
    double volumeTotal = 0;
    std::vector<PontosInter> pIntersecoes;

    for (CorPosicao &pixel : telaPixels) {
        try {
            pIntersecoes = no->smc(pixel.posicao);

            for (PontosInter &pi : pIntersecoes) {
                volumeTotal += (pi.fim - pi.inicio) * area;
            }

            pIntersecoes.clear();
        } catch (int erro) {
            continue;
        }
    }

    return volumeTotal;
}

#endif