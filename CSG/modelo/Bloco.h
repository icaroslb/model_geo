#ifndef BLOCO_H
#define BLOCO_H

#include "No.h"

class Bloco : public virtual No {
private:
    double largura, altura, profundidade;
    Eigen::Vector3d centro,
                    cor,
                    p1,
                    p2,
                    p3,
                    p4,
                    p5,
                    p6,
                    p7,
                    p8;

public:
    Bloco (Eigen::Vector3d centro, double largura, double altura, double profundidade, Eigen::Vector3d cor);

    double& obterLargura ();
    double& obterAltura ();
    double& obterProfundidade ();
    double& obterMaiorDimensao ();
    Eigen::Vector3d& obterCentro ();

    Intersec intersecao (Eigen::Vector3d vetor);
    Eigen::Vector3d& obterCor ();
    Eigen::Vector3d obterNormal (Eigen::Vector3d ponto);
    Eigen::Vector3d obterNormal (int idFace);
    std::vector<PontosInter> smc (Eigen::Vector3d vetor);

    void mover (Eigen::Vector3d movimento);
    void escalonar (double x, double y, double z);

    double volume ();
    void obterFace (int idFace, Eigen::Vector3d &pf1, Eigen::Vector3d &pf2, Eigen::Vector3d &pf3, Eigen::Vector3d &pf4);
};

#endif