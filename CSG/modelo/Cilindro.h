#ifndef CILINDRO_H
#define CILINDRO_H

#include "No.h"

class Cilindro : No {
    Eigen::Vector3d ponta1,
                    centro,
                    ponta2,
                    vAlturaU,
                    cor;
    double raio,
           altura;
    
    Cilindro (Eigen::Vector3d centro, double raio, double altura, Eigen::Vector3d cor);

    Intersec intersecao (Eigen::Vector3d vetor);
    Eigen::Vector3d& obterCor ();
    Eigen::Vector3d obterNormal (Eigen::Vector3d ponto);
    std::vector<PontosInter> smc (Eigen::Vector3d ponto);
    void mover (Eigen::Vector3d movimento);
    void escalonar (double x, double y, double z);

    double volume ();
};

#endif