#include "Arvore.h"
#include <iostream>
Arvore::Arvore (int tipo, No *filho1, No *filho2, Eigen::Vector3d cor) :
tipo(tipo),
filho1(filho1),
filho2(filho2),
cor(cor) { }

Intersec Arvore::intersecao (Eigen::Vector3d vetor) {
    Intersec intersec1,
             intersec2;
    bool erro1 = false,
         erro2 = false;
    
    try {
        intersec1 = filho1->intersecao(vetor);
    } catch (int erro) {
        erro1 = true;
        intersec1.dist = INFINITY;
    }
    
    try {
        intersec2 = filho2->intersecao(vetor);
    } catch (int erro) {
        erro2 = true;
        intersec2.dist = INFINITY;
    }
    
    if (erro1 && erro2) {
        throw(SEM_INTERCESSAO);
    } else {
        return (intersec1.dist < intersec2.dist) ? intersec1 : intersec2;
    }
}

Eigen::Vector3d& Arvore::obterCor () {
    return cor;
}

Eigen::Vector3d Arvore::obterNormal (Eigen::Vector3d ponto) {
    throw (ERRO_ARVORE_NORMAL);
}

std::vector<PontosInter> Arvore::smc (Eigen::Vector3d ponto) {
    std::vector<PontosInter> intersecoesOperador1,
                             intersecoesOperador2,
                             intersecoes;
    int tamanho,
        analise,
        i = 0,
        j = 0;

    try {
        intersecoesOperador1 = filho1->smc(ponto);
    } catch (int erro) {}
    
    try {
        intersecoesOperador2 = filho2->smc(ponto);
    } catch (int erro) {}
    
    tamanho = (intersecoesOperador1.size() > intersecoesOperador2.size()) ? intersecoesOperador1.size() : intersecoesOperador2.size();

    while (i < intersecoesOperador1.size() || j < intersecoesOperador2.size()) {
        if (i < intersecoesOperador1.size() && j < intersecoesOperador2.size()) {
        analise = analisarPontosIntersecao(intersecoesOperador1[i], intersecoesOperador2[j]);
            if (analise == IGUAIS) {
                intersecoes.push_back(intersecoesOperador1[i]);
                i++;
                j++;
            } else if (analise == ENTRE_1_DENTRO) {
                intersecoes.push_back(intersecoesOperador2[j]);
                i++;
                j++;
            } else if (analise == ENTRE_2_DENTRO) {
                intersecoes.push_back(intersecoesOperador1[i]);
                i++;
                j++;
            } else if (analise == ENTRE_DISJUNTOS_1_MENOR) {
                intersecoes.push_back(PontosInter(intersecoesOperador1[i].inicio, intersecoesOperador2[j].fim));
                i++;
                j++;
            } else if (analise == ENTRE_DISJUNTOS_2_MENOR) {
                intersecoes.push_back(PontosInter(intersecoesOperador2[j].inicio, intersecoesOperador1[i].fim));
                i++;
                j++;
            } else if (analise == DISJUNTOS_1_MENOR) {
                intersecoes.push_back(intersecoesOperador1[i]);
                i++;
            } else {
                intersecoes.push_back(intersecoesOperador2[j]);
                j++;
            }
        } else if (i < intersecoesOperador1.size()) {
            intersecoes.push_back(intersecoesOperador1[i]);
            i++;
        } else {
            intersecoes.push_back(intersecoesOperador2[j]);
            j++;
        }
    }

    return intersecoes;
}

void Arvore::mover (Eigen::Vector3d movimento) {
    filho1->mover(movimento);
    filho2->mover(movimento);
}

void Arvore::escalonar (double x, double y, double z) {
    filho1->escalonar(x, y, z);
    filho2->escalonar(x, y, z);
}

double Arvore::volume () {

}