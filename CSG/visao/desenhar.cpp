#include "desenhar.h"
#include <iostream>

static std::mutex pintor,
           escolherPosicao;
static std::vector <CorPosicao> telaPixels;
static std::vector <bool> indiceTerminado;
static int indice,
           tamanho;

static GLuint textura;

static double alturaPix,
              larguraPix;

void inicializarTela (int altura, int largura, int dimAltura, int dimLargura) {
    double inicioLargura = largura/2.0,
           inicioAltura = altura/2.0;
    int i = 0,
        lin,
        col;

    alturaPix = (double)altura / (double)dimAltura,
    larguraPix = (double)largura / (double)dimLargura;

    tamanho = dimLargura * dimAltura;
    telaPixels.resize(tamanho);
    for (CorPosicao &tp : telaPixels) {
        lin = i / dimLargura;
        col = i % dimLargura;

        tp.posicao.x() = -inicioLargura + (larguraPix / 2.0) + (col * larguraPix);
        tp.posicao.y() = -inicioAltura + (alturaPix / 2.0) + (lin * alturaPix);
        tp.posicao.z() = 1;
        i++;
    }
}

void desenhar (int dimAltura, int dimLargura, std::vector<No*> &objetos) {
    Eigen::Vector3d posicao(0.0, 0.0, 1.0),
                    cor(0.0, 0.0, 0.0);
    std::vector<std::thread> ts;
    int qtd_threads = std::thread::hardware_concurrency(),
        indiceAux = 0;
    
    indice = 0;
    indiceTerminado.resize(tamanho, false);

    for (int i = 0; i < qtd_threads; i++) {
        ts.push_back(std::thread(pintar, dimLargura, dimAltura, std::ref(objetos)));
    }
    
    glBegin(GL_POINTS);
        for (CorPosicao &cp : telaPixels) {
            while (!indiceTerminado[indiceAux]) {
                std::this_thread::yield();
            }
            glColor3dv(cp.cor.data());
            glVertex2dv(cp.posicao.data());
            indiceAux++;
        }
    glEnd();
    
    for (std::thread &t : ts) {
        t.join();
    }
}

void pintar (int dimLargura, int dimAltura, std::vector<No*> &objetos) {
    Eigen::Vector3d posicao,
                    cor(0.0, 0.0, 0.0);
    int i,
        lin,
        col;

    escolherPosicao.lock();
    i = indice++;
    escolherPosicao.unlock();
    while (i < tamanho) {        
        posicao = telaPixels[i].posicao.normalized();
        
        cor = calcularCor(posicao, objetos);

        pintor.lock();
        telaPixels[i].cor = cor;
        pintor.unlock();

        escolherPosicao.lock();
        indiceTerminado[i] = true;
        i = indice++;
        escolherPosicao.unlock();
    }
}

Eigen::Vector3d calcularCor (Eigen::Vector3d posicao, std::vector<No*> &objetos) {
    Intersec distancia,
             menorDistancia;
    No *olhando = nullptr;
    Eigen::Vector3d material,
                    pos,
                    global = Eigen::Vector3d(0.2, 0.2, 0.2),
                    difusa = Eigen::Vector3d (0.5, 0.5, 0.5),
                    especular = Eigen::Vector3d (0.5, 0.5, 0.5),
                    retorno,
                    normal;

    menorDistancia.dist = INFINITY;
    for (No *o : objetos) {
        try {
            distancia = o->intersecao(posicao);
            if (distancia.dist >= 0 && menorDistancia.dist > distancia.dist) {
                menorDistancia = distancia;
                olhando = o;
            }
        } catch (int erro) {
            continue;
        }
    }
    
    if (olhando != nullptr) {
        material = olhando->obterCor();

        retorno.x() = material.x() * global.x();
        retorno.y() = material.y() * global.y();
        retorno.z() = material.z() * global.z();

        return retorno +
               iDifusa(difusa, posicao * menorDistancia.dist, menorDistancia.normal) +
               iEspecular(especular, posicao * menorDistancia.dist, menorDistancia.normal, posicao);
    } else {
        return Eigen::Vector3d(0.0, 0.0, 0.0);
    }
}

Eigen::Vector3d iDifusa (Eigen::Vector3d coefDif, Eigen::Vector3d posicao, Eigen::Vector3d normal) {
    Eigen::Vector3d posLuz(0.0, 0.0, -5.0),
                    forcLuz(0.5, 0.5, 0.5),
                    vLuz = (posLuz - posicao).normalized();
    double cosseno = normal.dot(vLuz);
    if (cosseno >= 0) {
        return coefDif * cosseno;
    } else {
        return Eigen::Vector3d(0.0, 0.0, 0.0);
    }
}

Eigen::Vector3d iEspecular (Eigen::Vector3d coefEspec, Eigen::Vector3d posicao, Eigen::Vector3d normal, Eigen::Vector3d observador) {
    Eigen::Vector3d posLuz(0.0, 0.0, -5.0),
                    forcLuz(0.5, 0.5, 0.5),
                    vetorObservador = (posicao - observador).normalized(),
                    vLuz = (posLuz - posicao).normalized(),
                    vetorEspec = ((vLuz + observador)).normalized();
    double cosseno = normal.dot(vLuz);

    if (cosseno >= 0) {
        return (coefEspec * cosseno) * pow((2 * normal.dot(vLuz) * normal.dot(vetorObservador) - vetorObservador.dot(vLuz)), 10);
    } else {
        return Eigen::Vector3d(0.0, 0.0, 0.0);
    }
}

std::vector <CorPosicao>& obterPixels () {
    return telaPixels;
}

double obterArea () {
    return 0.125;
}