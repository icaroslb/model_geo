#include <vector>
#include <limits>
#include <thread>
#include <mutex>

#include "Pintor.h"
#include "../modelo/Arvore.h"
#include "../modelo/Esfera.h"
#include "../modelo/Bloco.h"

void inicializarTela (int altura, int largura, int dimAltura, int dimLargura);
void desenhar (int dimAltura, int dimLargura, std::vector<No*> &objetos);
void pintar (int dimLargura, int dimAltura, std::vector<No*> &objetos);
Eigen::Vector3d calcularCor (Eigen::Vector3d posicao, std::vector<No*> &objetos);
Eigen::Vector3d iDifusa (Eigen::Vector3d coefDif, Eigen::Vector3d posicao, Eigen::Vector3d normal);
Eigen::Vector3d iEspecular (Eigen::Vector3d coefEspec, Eigen::Vector3d posicao, Eigen::Vector3d normal, Eigen::Vector3d observador);
std::vector <CorPosicao>& obterPixels ();
double obterArea ();