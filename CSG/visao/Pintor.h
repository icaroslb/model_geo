#ifndef PINTOR_H
#define PINTOR_H

#include <GL/gl.h>

class Pintor {
public:
    Pintor ();
    void pintar (double *cor, double *posicao);
};

#endif